# Microsoft Developer Studio Generated NMAKE File, Format Version 4.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

!IF "$(CFG)" == ""
CFG=3dRCE_He - Win32 Debug
!MESSAGE No configuration specified.  Defaulting to 3dRCE_He - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "3dRCE_He - Win32 Release" && "$(CFG)" !=\
 "3dRCE_He - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "3dRCE_He.mak" CFG="3dRCE_He - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "3dRCE_He - Win32 Release" (based on\
 "Win32 (x86) Console Application")
!MESSAGE "3dRCE_He - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "3dRCE_He - Win32 Debug"
RSC=rc.exe
F90=fl32.exe

!IF  "$(CFG)" == "3dRCE_He - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
OUTDIR=.\Release
INTDIR=.\Release

ALL : "$(OUTDIR)\3dRCE_He.exe"

CLEAN : 
	-@erase ".\Release\3dRCE_He.exe"
	-@erase ".\Release\main.obj"
	-@erase ".\Release\States.mod"
	-@erase ".\Release\EXCH4.OBJ"
	-@erase ".\Release\amps.obj"
	-@erase ".\Release\Global.mod"
	-@erase ".\Release\states.obj"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Ox /I "Release/" /c /nologo
# ADD F90 /Ox /4R8 /I "Release/" /c /nologo
F90_PROJ=/Ox /4R8 /I "Release/" /c /nologo /Fo"Release/" 
F90_OBJS=.\Release/
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/3dRCE_He.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)/3dRCE_He.pdb" /machine:I386 /out:"$(OUTDIR)/3dRCE_He.exe" 
LINK32_OBJS= \
	"$(INTDIR)/main.obj" \
	"$(INTDIR)/EXCH4.OBJ" \
	"$(INTDIR)/amps.obj" \
	"$(INTDIR)/states.obj"

"$(OUTDIR)\3dRCE_He.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "3dRCE_He - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "$(OUTDIR)\3dRCE_He.exe"

CLEAN : 
	-@erase ".\Debug\3dRCE_He.exe"
	-@erase ".\Debug\amps.obj"
	-@erase ".\Debug\global.mod"
	-@erase ".\Debug/States.mod"
	-@erase ".\Debug\main.obj"
	-@erase ".\Debug\EXCH4.OBJ"
	-@erase ".\Debug\states.obj"
	-@erase ".\Debug\3dRCE_He.ilk"
	-@erase ".\Debug\3dRCE_He.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Zi /I "Debug/" /c /nologo
# ADD F90 /4R8 /Zi /I "Debug/" /c /nologo
F90_PROJ=/4R8 /Zi /I "Debug/" /c /nologo /Fo"Debug/" /Fd"Debug/3dRCE_He.pdb" 
F90_OBJS=.\Debug/
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/3dRCE_He.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)/3dRCE_He.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)/3dRCE_He.exe" 
LINK32_OBJS= \
	"$(INTDIR)/amps.obj" \
	"$(INTDIR)/main.obj" \
	"$(INTDIR)/EXCH4.OBJ" \
	"$(INTDIR)/states.obj"

"$(OUTDIR)\3dRCE_He.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.for{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f90{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

################################################################################
# Begin Target

# Name "3dRCE_He - Win32 Release"
# Name "3dRCE_He - Win32 Debug"

!IF  "$(CFG)" == "3dRCE_He - Win32 Release"

!ELSEIF  "$(CFG)" == "3dRCE_He - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\states.f90

!IF  "$(CFG)" == "3dRCE_He - Win32 Release"

F90_MODOUT=\
	"States"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\states.obj" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\States.mod" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "3dRCE_He - Win32 Debug"

F90_MODOUT=\
	"States"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\states.obj" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\states.mod" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\main.f90

!IF  "$(CFG)" == "3dRCE_He - Win32 Release"

NODEP_F90_MAIN_=\
	".\Release\States.mod"\
	
F90_MODOUT=\
	"Global"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\main.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\States.mod"
   $(BuildCmds)

"$(INTDIR)\Global.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\States.mod"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "3dRCE_He - Win32 Debug"

DEP_F90_MAIN_=\
	".\Debug/States.mod"\
	
F90_MODOUT=\
	"Global"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\main.obj" : $(SOURCE) $(DEP_F90_MAIN_) "$(INTDIR)"\
 "$(INTDIR)\states.mod"
   $(BuildCmds)

"$(INTDIR)\global.mod" : $(SOURCE) $(DEP_F90_MAIN_) "$(INTDIR)"\
 "$(INTDIR)\states.mod"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\EXCH4.FOR

"$(INTDIR)\EXCH4.OBJ" : $(SOURCE) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\amps.f90

!IF  "$(CFG)" == "3dRCE_He - Win32 Release"

NODEP_F90_AMPS_=\
	".\Release\Global.mod"\
	".\Release\States.mod"\
	

"$(INTDIR)\amps.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Global.mod"\
 "$(INTDIR)\States.mod"


!ELSEIF  "$(CFG)" == "3dRCE_He - Win32 Debug"

NODEP_F90_AMPS_=\
	".\Debug\global.mod"\
	".\Debug/States.mod"\
	

"$(INTDIR)\amps.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\global.mod"\
 "$(INTDIR)\states.mod"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\input.txt

!IF  "$(CFG)" == "3dRCE_He - Win32 Release"

!ELSEIF  "$(CFG)" == "3dRCE_He - Win32 Debug"

!ENDIF 

# End Source File
# End Target
# End Project
################################################################################
