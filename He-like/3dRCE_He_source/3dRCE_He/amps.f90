SUBROUTINE CalcFklm
!---------------------------------------------------------------------------------------
! ��� ������� ��������� ���������� ����������� ��������, �������� �������� �������
! � ������������ ������� � ������������ �����-����������
!---------------------------------------------------------------------------------------
USE Global
Implicit None

integer :: n
real :: Gabs

complex StructureFactor
real U0fourier

do n = 1, n_res
	strf(n) = StructureFactor( res_index(n, 1), res_index(n, 2), res_index(n, 3))
	G_klm(n, 1) = akoeff * dsqrt(2.) * res_index(n, 1)
	G_klm(n, 2) = akoeff * res_index(n, 2)
	G_klm(n, 3) = akoeff * dsqrt(2.) * res_index(n, 3)
	Gabs = dsqrt( G_klm(n, 1)**2 + G_klm(n, 2)**2 + G_klm(n, 3)**2 )
	Fklm(n) = akoeff**3 * strf(n) * dexp( - Gabs**2 * r_sqr_mean/6.) * U0fourier(Gabs) 	
enddo

!---------------------------------------------------------------------------------------
END SUBROUTINE CalcFklm

SUBROUTINE CalcResAmp
!-----------------------------------------------------------------------------------------------------------------------------------
! ���������� ��������� ��������� ��� ������ ���������
!-----------------------------------------------------------------------------------------------------------------------------------
USE Global
USE States
Implicit None

integer :: p, q, i, j, n
complex :: res, a1, a2
real :: Z1, Z2
real :: deltas1, deltas2
real D

! �������������� �������� �������� ������� � ������� ����, ���������� �����������
do n = 1, n_res
	G1_klm(n, 1) = gamma*G_klm(n, 1)
	G1_klm(n, 2) = G_klm(n, 2)
	G1_klm(n, 3) = G_klm(n, 3)
enddo

! ��������� �������� ������� ���� ����� ����������� ������
Amp1 = (0., 0.)
do n = 1, n_res
	do p = 1, nbasis
		do q = 1, nbasis
				
			res = (0., 0.)
			do i = 1, Nconf
				do j = 1, Nconf
					if ( st_basis(p)%conf_exp(i) .ne. 0 .and. st_basis(q)%conf_exp(j) .ne. 0 ) then
					
						! ������ ��������
						Z1 = 0.0
						Z2 = 0.0
						if( conf_list(st_basis(p)%conf_exp(i))%q1(1) .eq. st_basis(p)%n1  )	 then
							Z1 = st_basis(p)%Z1
						endif
						if( conf_list(st_basis(p)%conf_exp(i))%q1(1) .eq. st_basis(p)%n2  )  then
							Z1 = st_basis(p)%Z2
						endif
						if( conf_list(st_basis(q)%conf_exp(j))%q1(1) .eq. st_basis(q)%n1  )	 then
							Z2 = st_basis(q)%Z1
						endif
						if( conf_list(st_basis(q)%conf_exp(j))%q1(1) .eq. st_basis(q)%n2  )  then
							Z2 = st_basis(q)%Z2
						endif
						if( Z1.eq.0.0 .or. Z2.eq.0.0 ) then
							print*, 'error!'
							stop
						endif
						
						CALL pw_me(G1_klm(n, :), & 
						Z1, conf_list(st_basis(p)%conf_exp(i))%q1(1), conf_list(st_basis(p)%conf_exp(i))%q1(2), & 
																	  conf_list(st_basis(p)%conf_exp(i))%q1(3), &   
						Z2, conf_list(st_basis(q)%conf_exp(j))%q1(1), conf_list(st_basis(q)%conf_exp(j))%q1(2), &
																	  conf_list(st_basis(q)%conf_exp(j))%q1(3), &
																												a1)
					
						! ������ ��������
						Z1 = 0.0
						Z2 = 0.0
						if( conf_list(st_basis(p)%conf_exp(i))%q2(1) .eq. st_basis(p)%n1  )	 then
							Z1 = st_basis(p)%Z1
						endif
						if( conf_list(st_basis(p)%conf_exp(i))%q2(1) .eq. st_basis(p)%n2  )  then
							Z1 = st_basis(p)%Z2
						endif
						if( conf_list(st_basis(q)%conf_exp(j))%q2(1) .eq. st_basis(q)%n1  )	 then
							Z2 = st_basis(q)%Z1
						endif
						if( conf_list(st_basis(q)%conf_exp(j))%q2(1) .eq. st_basis(q)%n2  )  then
							Z2 = st_basis(q)%Z2
						endif
						if( Z1.eq.0.0 .or. Z2.eq.0.0 ) then
							print*, 'error!'
							stop
						endif

						CALL pw_me(G1_klm(n, :), & 
						Z1, conf_list(st_basis(p)%conf_exp(i))%q2(1), conf_list(st_basis(p)%conf_exp(i))%q2(2), & 
																	  conf_list(st_basis(p)%conf_exp(i))%q2(3), &   
						Z2, conf_list(st_basis(q)%conf_exp(j))%q2(1), conf_list(st_basis(q)%conf_exp(j))%q2(2), &	
																	  conf_list(st_basis(q)%conf_exp(j))%q2(3), &
																												a2)


						deltas1 = D( conf_list(st_basis(p)%conf_exp(i))%q2(1),  conf_list(st_basis(q)%conf_exp(j))%q2(1) ) * &	!< ������-������� �� ������� ���������
								  D( conf_list(st_basis(p)%conf_exp(i))%q2(2),  conf_list(st_basis(q)%conf_exp(j))%q2(2) ) * &	!
								  D( conf_list(st_basis(p)%conf_exp(i))%q2(3),  conf_list(st_basis(q)%conf_exp(j))%q2(3) ) * &	!
								  D( conf_list(st_basis(p)%conf_exp(i))%q2(4),  conf_list(st_basis(q)%conf_exp(j))%q2(4) ) * &	!------------------------------------------
								  D( conf_list(st_basis(p)%conf_exp(i))%q1(4), 	conf_list(st_basis(q)%conf_exp(j))%q1(4) )		!< ������-������ �� ����� ������� ���������


						deltas2 = D( conf_list(st_basis(p)%conf_exp(i))%q1(1),  conf_list(st_basis(q)%conf_exp(j))%q1(1) ) * &	!< ������-������� �� ������� ���������
								  D( conf_list(st_basis(p)%conf_exp(i))%q1(2),  conf_list(st_basis(q)%conf_exp(j))%q1(2) ) * &	!
								  D( conf_list(st_basis(p)%conf_exp(i))%q1(3),  conf_list(st_basis(q)%conf_exp(j))%q1(3) ) * &	!
								  D( conf_list(st_basis(p)%conf_exp(i))%q1(4),  conf_list(st_basis(q)%conf_exp(j))%q1(4) ) * &	!------------------------------------------
								  D( conf_list(st_basis(p)%conf_exp(i))%q2(4), 	conf_list(st_basis(q)%conf_exp(j))%q2(4) )		!< ������-������ �� ����� ������� ���������
						
						res = res + st_basis(p)%coeffs_exp(i) * st_basis(q)%coeffs_exp(j) * & 
									( deltas1*a1 + deltas2*a2 )
					endif
				enddo
			enddo
		
			Amp1(n, p, q) = res
			
		enddo

	enddo
enddo

! ��������� �������� exp(igr) d/dx ����� ����������� ������
Amp2 = (0., 0.)
do n = 1, n_res
	do p = 1, nbasis
		do q = 1, nbasis
			
			res = (0., 0.)
			do i = 1, Nconf
				do j = 1, Nconf
					if ( st_basis(p)%conf_exp(i) .ne. 0 .and. st_basis(q)%conf_exp(j) .ne. 0 ) then
					
						! ������ ��������
						Z1 = 0.0
						Z2 = 0.0
						if( conf_list(st_basis(p)%conf_exp(i))%q1(1) .eq. st_basis(p)%n1  )	 then
							Z1 = st_basis(p)%Z1
						endif
						if( conf_list(st_basis(p)%conf_exp(i))%q1(1) .eq. st_basis(p)%n2  )  then
							Z1 = st_basis(p)%Z2
						endif
						if( conf_list(st_basis(q)%conf_exp(j))%q1(1) .eq. st_basis(q)%n1  )	 then
							Z2 = st_basis(q)%Z1
						endif
						if( conf_list(st_basis(q)%conf_exp(j))%q1(1) .eq. st_basis(q)%n2  )  then
							Z2 = st_basis(q)%Z2
						endif
						if( Z1.eq.0.0 .or. Z2.eq.0.0 ) then
							print*, 'error!'
							stop
						endif
						
						CALL pw_ddx_me(G1_klm(n, :), & 
						Z1, conf_list(st_basis(p)%conf_exp(i))%q1(1), conf_list(st_basis(p)%conf_exp(i))%q1(2), & 
																	  conf_list(st_basis(p)%conf_exp(i))%q1(3), &   
						Z2, conf_list(st_basis(q)%conf_exp(j))%q1(1), conf_list(st_basis(q)%conf_exp(j))%q1(2), &
																	  conf_list(st_basis(q)%conf_exp(j))%q1(3), &
																												a1)
					
						! ������ ��������
						Z1 = 0.0
						Z2 = 0.0
						if( conf_list(st_basis(p)%conf_exp(i))%q2(1) .eq. st_basis(p)%n1  )	 then
							Z1 = st_basis(p)%Z1
						endif
						if( conf_list(st_basis(p)%conf_exp(i))%q2(1) .eq. st_basis(p)%n2  )  then
							Z1 = st_basis(p)%Z2
						endif
						if( conf_list(st_basis(q)%conf_exp(j))%q2(1) .eq. st_basis(q)%n1  )	 then
							Z2 = st_basis(q)%Z1
						endif
						if( conf_list(st_basis(q)%conf_exp(j))%q2(1) .eq. st_basis(q)%n2  )  then
							Z2 = st_basis(q)%Z2
						endif
						if( Z1.eq.0.0 .or. Z2.eq.0.0 ) then
							print*, 'error!'
							stop
						endif

						CALL pw_ddx_me(G1_klm(n, :), & 
						Z1, conf_list(st_basis(p)%conf_exp(i))%q2(1), conf_list(st_basis(p)%conf_exp(i))%q2(2), & 
																	  conf_list(st_basis(p)%conf_exp(i))%q2(3), &   
						Z2, conf_list(st_basis(q)%conf_exp(j))%q2(1), conf_list(st_basis(q)%conf_exp(j))%q2(2), &	
																	  conf_list(st_basis(q)%conf_exp(j))%q2(3), &
																												a2)


						deltas1 = D( conf_list(st_basis(p)%conf_exp(i))%q2(1),  conf_list(st_basis(q)%conf_exp(j))%q2(1) ) * &	!< ������-������� �� ������� ���������
								  D( conf_list(st_basis(p)%conf_exp(i))%q2(2),  conf_list(st_basis(q)%conf_exp(j))%q2(2) ) * &	!
								  D( conf_list(st_basis(p)%conf_exp(i))%q2(3),  conf_list(st_basis(q)%conf_exp(j))%q2(3) ) * &	!
								  D( conf_list(st_basis(p)%conf_exp(i))%q2(4),  conf_list(st_basis(q)%conf_exp(j))%q2(4) ) * &	!------------------------------------------
								  D( conf_list(st_basis(p)%conf_exp(i))%q1(4), 	conf_list(st_basis(q)%conf_exp(j))%q1(4) )		!< ������-������ �� ����� ������� ���������


						deltas2 = D( conf_list(st_basis(p)%conf_exp(i))%q1(1),  conf_list(st_basis(q)%conf_exp(j))%q1(1) ) * &	!< ������-������� �� ������� ���������
								  D( conf_list(st_basis(p)%conf_exp(i))%q1(2),  conf_list(st_basis(q)%conf_exp(j))%q1(2) ) * &	!
								  D( conf_list(st_basis(p)%conf_exp(i))%q1(3),  conf_list(st_basis(q)%conf_exp(j))%q1(3) ) * &	!
								  D( conf_list(st_basis(p)%conf_exp(i))%q1(4),  conf_list(st_basis(q)%conf_exp(j))%q1(4) ) * &	!------------------------------------------
								  D( conf_list(st_basis(p)%conf_exp(i))%q2(4), 	conf_list(st_basis(q)%conf_exp(j))%q2(4) )		!< ������-������ �� ����� ������� ���������
						
						res = res + st_basis(p)%coeffs_exp(i) * st_basis(q)%coeffs_exp(j) * & 
									( deltas1*a1 + deltas2*a2 )
					endif
				enddo
			enddo
		
			Amp2(n, p, q) = res
		enddo
	enddo
enddo
!---------------------------------------------------------------------------------------------------------------------------------
END SUBROUTINE CalcResAmp


REAL FUNCTION D(i, j)
!-------------------------------
! ������ ���������
!-------------------------------
integer :: i, j

if(i .eq. j) D = 1.0
if(i .ne. j ) D = 0.0
!-------------------------------
END FUNCTION D


COMPLEX FUNCTION StructureFactor( k, l, m )
!=========================================================================================
use Global
integer :: k, l, m

StructureFactor = 2.*(1. + (-iunit)**(2.*m+l))*(1.+(-1.)**(k+l+m))

!=========================================================================================
END FUNCTION StructureFactor


SUBROUTINE VectorAngles(KX, KY, KZ, K, THETA, PHI)
!==============================================================================================================
! �������� �������� ������� �� ��� �������� ������ � ���� � ����������� ������� 
!==============================================================================================================
REAL, PARAMETER :: PI = 3.141592653589793
REAL, INTENT(IN) :: KX, KY, KZ
REAL, INTENT(OUT) :: K, THETA, PHI

K=DSQRT( KX**2 + KY**2 + KZ**2 )
IF (KX.GT.0.0.AND.KY.GE.0.0) THEN
	PHI=DATAN(KY/KX)
ENDIF
IF (KX.GT.0.0.AND.KY.LT.0.0) THEN
	PHI=2*PI - DATAN(DABS(KY/KX))
ENDIF
IF (KX.LT.0.0.AND.KY.GT.0.0) THEN
	PHI=PI - DATAN(DABS(KY/KX))
ENDIF
IF (KX.LT.0.0.AND.KY.LE.0.0) THEN
	PHI=PI + DATAN(DABS(KY/KX))
ENDIF
IF (KX.EQ.0.0.AND.KY.GT.0.0) THEN
	PHI=PI/2.
ENDIF
IF (KX.EQ.0.0.AND.KY.LT.0.0) THEN
	PHI=3.*PI/2.
ENDIF
IF (KX.EQ.0.0.AND.KY.EQ.0.0) THEN
	PHI=0.0
ENDIF

IF (KZ.GT.0.0) THEN
	THETA=DATAN( DSQRT( KX**2 + KY**2 )/KZ )
ENDIF
IF (KZ.LT.0.0) THEN
	THETA= PI - DATAN( DSQRT( KX**2 + KY**2 )/DABS(KZ) )
ENDIF
IF (KZ.EQ.0) THEN
	THETA=PI/2.
ENDIF

RETURN
!==============================================================================================================
END SUBROUTINE VectorAngles


RECURSIVE SUBROUTINE pw_ddx_me(q, Z1, n1, l1, m1, Z2, n2, l2, m2, Result)
!==================================================================================================
! ��������� �������� 																			  =
!																								  =
! < n1, l1, m1| e^{i\vec{q}\vec{r}}  d/dx  |n2, l2, m2 >										  =
!																								  =
! � �������� �������� n=3																		  =
!																								  =
! ���������� ������� � ������ � ����� ��������� ����� ���� ����� ��� ������ �������				  =
! Z1 - ����� ��� ������ ��������																  =
! Z2 - ����� ��� ����� ��������																	  =
!																								  =
!																								  =
! ������������ ������������ ���������� ��������� ��������� �� ������� ����						  =
! SUBROUTINE pw_me: <n1 l1 m2| exp(i \vec{q} \vec{r}|n2 l2 m2>									  = 
!																								  =
!																								  =
! ��������� ��������� ��� ���� �������, ������� ������� ������������							  =
!==================================================================================================
implicit none
real, intent(in), dimension(3) :: q
real, intent(in) :: Z1, Z2
integer, intent(in) :: n1, l1, m1
integer, intent(in) :: n2, l2, m2
complex, intent(out) :: Result
real*8, parameter :: pi = 3.1415926535897932385
complex*8, parameter :: iunit = (0.D0, 1.D0)

real :: q_inverse(3)
real :: qabs, theta_q, phi_q
integer :: k1, k2
complex :: res1, res2
integer :: lambda, mu
real :: Rad1, Rad2, Rad3, Rad4
real :: RadialIntType1, RadialIntType2
real :: sphInt1, sphInt2, sphInt3, sphInt4
real :: a1, a2, a3, a4
complex :: sphfunc1, sphfunc2, sphfunc3, sphfunc4
complex :: SPHER
real :: a1coeff, a2coeff, a3coeff, a4coeff
real :: ThreeSpherHarmonicIntegral

k1 = 100*n1 + l1
k2 = 100*n2 + l2

CALL VectorAngles( q(1), q(2), q(3), qabs, theta_q, phi_q )

Result = (0., 0.)

! ������� ������������
if(k2 .lt. k1) then
	
	q_inverse = - q

	CALL pw_me( q_inverse, Z2, n2, l2, m2, Z1, n1, l1, m1, res1)
	
	CALL pw_ddx_me( q_inverse, Z2, n2, l2, m2, Z1, n1, l1, m1, res2)
	
	RESULT = - iunit * q(1) *dconjg( res1 ) - dconjg( res2 )
	
else

	a1 = a1coeff(l2, m2)
	a2 = a2coeff(l2, m2)
	a3 = a3coeff(l2, m2)
	a4 = a4coeff(l2, m2)


	do lambda = 0, 5
		
		sphfunc1 = 0.0
		sphfunc2 = 0.0
		sphfunc3 = 0.0 
		sphfunc4 = 0.0
		Rad1 = 0.0
		Rad2 = 0.0
		Rad3 = 0.0
		Rad4 = 0.0


		if( a1 .ne. 0.0 ) then
			mu = m1 - m2 + 1
			sphInt1 = ThreeSpherHarmonicIntegral(l1, m1, lambda, mu, l2 + 1, m2 - 1) 
			if( sphInt1 .ne. 0.0 ) then
				Rad1 = RadialIntType1(Z1, n1, l1, Z2, n2, l2, qabs, lambda)
				sphfunc1 = dconjg(SPHER(lambda, mu, theta_q, phi_q))
			endif
		endif
			
		if( a2 .ne. 0.0 ) then
			mu = m1 - m2 + 1
			sphInt2 = ThreeSpherHarmonicIntegral(l1, m1, lambda, mu, l2 - 1, m2 - 1) 
			if( sphInt2 .ne. 0.0 ) then
				Rad2 = RadialIntType2(Z1, n1, l1, Z2, n2, l2, qabs, lambda)
				sphfunc2 = dconjg(SPHER(lambda, mu, theta_q, phi_q))
			endif
		endif

		if( a3 .ne. 0.0 ) then
			mu = m1 - m2 - 1
			sphInt3 = ThreeSpherHarmonicIntegral(l1, m1, lambda, mu, l2 + 1, m2 + 1) 
			if( sphInt3 .ne. 0.0 ) then
				Rad3 = RadialIntType1(Z1, n1, l1, Z2, n2, l2, qabs, lambda)
				sphfunc3 = dconjg(SPHER(lambda, mu, theta_q, phi_q))
			endif
		endif
			
		if( a4 .ne. 0.0 ) then
			mu = m1 - m2 - 1
			sphInt4 = ThreeSpherHarmonicIntegral(l1, m1, lambda, mu, l2 - 1, m2 + 1) 
			if( sphInt4 .ne. 0.0 ) then
				Rad4 = RadialIntType2(Z1, n1, l1, Z2, n2, l2, qabs, lambda)
				sphfunc4 = dconjg(SPHER(lambda, mu, theta_q, phi_q))
			endif
		endif


	 	Result = Result + 4.*pi* iunit**lambda * 1./dsqrt(2.) * &
						  ( a1*Rad1*sphInt1*sphfunc1 - a2*Rad2*sphInt2*sphfunc2 - &
							a3*Rad3*sphInt3*sphfunc3 + a4*Rad4*sphInt4*sphfunc4 )
	enddo	

endif			   	 

RETURN
!==================================================================================================
END SUBROUTINE pw_ddx_me

REAL FUNCTION delta(k, m)
!-----------------------------------
! ������ ���������
!-----------------------------------
integer, intent(in) :: k, m

if( k.eq.m ) then
	delta = 1.0
else
	delta = 0.0
endif

!-----------------------------------
END FUNCTION delta

REAL FUNCTION ThreeSpherHarmonicIntegral(l, m, l1, m1, l2, m2)
!=======================================================================================
! 12.01.2007
! �������� �� ���� ����������� ��������
! ������� (�7.19) �.�. �������, �.�. ������� "���� ��������� ��������"
!
! ������������ ������� ������� CLEBSHG
! �� ������ ���� ������� ������ ���� ��������� ��������� TABULAR, �����������
! common-����� � ��������� ������������� ������-�������
!
! 15.01.2007
! ����������� � ������ �������� �� ������� ��������. 
! ������ ���������� c Wolfram Matematica 5
!=======================================================================================
REAL, PARAMETER :: PI = 3.14159265358979D0
INTEGER :: l, m, l1, m1, l2, m2 
REAL :: akoeff

akoeff = DSQRT( (2.D0*l1 + 1.D0) * (2.D0*l2 + 1.D0) / (4.D0*PI*(2.D0*l + 1.D0)) ) 
ThreeSpherHarmonicIntegral = akoeff * CLEBSHG(l1*2, 0, l2*2, 0, l*2, 0) & 
							* CLEBSHG(l1*2, m1*2, l2*2, m2*2, l*2, m*2)
RETURN
!=======================================================================================
END FUNCTION ThreeSpherHarmonicIntegral

! ������������
REAL FUNCTION a1coeff(l, m)
!=========================================================================================
integer, intent(in) :: l, m
real :: c

c = 1.0
if (l-m+1.lt.0) then
	c = 0.0
endif
if (l-m+2.lt.0) then
	c = 0.0
endif		

if( c.ne.0.0) then
	a1coeff = sqrt( (1.0*l - 1.0*m + 1.0)*(1.0*l - 1.0*m + 2.)/(2.*(2.*l + 1.)*(2.*l + 3.)) )
else
	a1coeff = 0.0
endif
!=========================================================================================
END FUNCTION a1coeff

REAL FUNCTION a2coeff(l, m)
!=========================================================================================
integer, intent(in) :: l, m
real :: c

c = 1.0
if (l+m-1.lt.0) then
	c = 0.0
endif
if (l+m.lt.0) then
	c = 0.0
endif		
if (2*l-1.le.0) then
	c = 0.0
endif		


if( c.ne.0.0) then
	a2coeff = sqrt( (1.0*l + 1.0*m - 1.0)*(1.0*l + 1.0*m)/(2.*(2.*l - 1.)*(2.*l + 1.)) )
else
	a2coeff = 0.0
endif

!=========================================================================================
END FUNCTION a2coeff

REAL FUNCTION a3coeff(l, m)
!=========================================================================================
integer, intent(in) :: l, m
real :: c

c = 1.0
if (l+m+1.lt.0) then
	c = 0.0
endif
if (l+m+2.lt.0) then
	c = 0.0
endif		

if( c.ne.0.0) then
	a3coeff = sqrt( (1.0*l + 1.0*m + 1.0)*(1.0*l + 1.0*m + 2.0)/(2.*(2.*l + 1.)*(2.*l + 3.)) )
else
	a3coeff = 0.0
endif

!=========================================================================================
END FUNCTION a3coeff

REAL FUNCTION a4coeff(l, m)
!=========================================================================================
integer, intent(in) :: l, m
real :: c

c = 1.0
if (l-m-1 .lt. 0) then
	c = 0.0	   
endif
if (l-m-2 .lt. 0) then
	c = 0.0
endif		
if (2*l-1 .le. 0) then
	c = 0.0
endif

if( c.ne.0.0) then
	a4coeff = sqrt( (1.0*l - 1.0*m - 1.0)*(1.0*l - 1.0*m)/(2.*(2.*l - 1.)*(2.*l + 1.)) )
else
	a4coeff = 0.0
endif

!=========================================================================================
END FUNCTION a4coeff



REAL FUNCTION RadialIntType1(Z1, n1, l1, Z2, n2, l2, q, lambda)
!=============================================================================================================================
! ���������� �������� ����
!
!  \int R_{n_1,l_1} j_\lambda(qr) \left( \frac{d R_{n_2,l_2}}{dr} - \frac{l_2}{r}R_{n_2,l_2} \right) r^2 dr
!
!
! ���������� ������ ��� ������ 
! n1<=n2 && l1<=l2
!
! ��� ������� ������ ������������ ������� ������������ � ����� ��������� ��������,
! ��� �������� � ������� ������.
!
!
! lambda �� 0 �� 5
!=============================================================================================================================
real, intent(in) :: Z1, Z2, q
integer, intent(in) :: n1, l1, n2, l2, lambda
integer :: k1, k2

k1 = 100*n1 + l1
k2 = 100*n2 + l2

if(k1.gt.k2) then
	print*, 'ERROR in A_ddx'
	STOP
endif

k1 = 100*n1 + l1
k2 = 100*n2 + l2

if(k1.gt.k2) print*, 'ERROR' 

! �������� �� 1s
	if( k1.eq.100 .and. k2.eq.100 ) then
		select case(lambda)
			case(0)
				RadialIntType1 = (-8.*Z1**1.5*Z2**2.5*(Z1 + Z2))/(q**2 + (Z1 + Z2)**2)**2
			case(1)
				RadialIntType1 = (-8.*q*Z1**1.5*Z2**2.5)/(q**2 + (Z1 + Z2)**2)**2
		end select
	endif

	if( k1.eq.100 .and. k2.eq.200 ) then
		select case(lambda)
			case(0)
				RadialIntType1 = (-8.*Sqrt(2.)*Z1**1.5*Z2**2.5*((4.*Z1 - Z2)*(2.*Z1 + Z2)**2 + 4.*q**2*(4.*Z1 + 3.*Z2)))/(4.*q**2 + (2.*Z1 + Z2)**2)**3	
			case(1)
				RadialIntType1 = (-32.*Sqrt(2.)*q*Z1**1.5*Z2**2.5*(4.*(q**2 + Z1**2) - Z2**2))/(4.*q**2 + (2.*Z1 + Z2)**2)**3
		end select
	endif

	if( k1.eq.100 .and. k2.eq.201 ) then
		select case(lambda)
			case(0)
				RadialIntType1 = (8.*Sqrt(2./3.)*Z1**1.5*Z2**3.5*(4.*q**2 - 3.*(2.*Z1 + Z2)**2))/(4.*q**2 + (2.*Z1 + Z2)**2)**3
			case(1)
				RadialIntType1 = (-64.*Sqrt(2./3.)*q*Z1**1.5*Z2**3.5*(2.*Z1 + Z2))/(4.*q**2 + (2.*Z1 + Z2)**2)**3
			case(2) 
				RadialIntType1 = (-128.*Sqrt(2./3.)*q**2*Z1**1.5*Z2**3.5)/(4.*q**2 + (2.*Z1 + Z2)**2)**3
		end select
	endif

	if( k1.eq.100 .and. k2.eq.300 ) then
		select case(lambda)
			case(0)
				RadialIntType1 = (-8.*Sqrt(3.)*Z1**1.5*Z2**2.5*(27.*q**4*(27.*Z1 + 19.*Z2) + (3.*Z1 + Z2)**3*(27.*Z1**2 - 12.*Z1*Z2 + Z2**2) +  &
							      6.*q**2*(243.*Z1**3 + 153.*Z1**2*Z2 - 15.*Z1*Z2**2 - 13.*Z2**3)))/(9.*q**2 + (3.*Z1 + Z2)**2)**4 
			case(1)
				RadialIntType1 = (-24.*Sqrt(3.)*q*Z1**1.5*Z2**2.5*(243.*q**4 + 18.*q**2*(27.*Z1**2 - 2.*Z1*Z2 - 5.*Z2**2) + &
                                 (3.*Z1 + Z2)**2*(27.*Z1**2 - 22.*Z1*Z2 + 3.*Z2**2)))/(9.*q**2 + (3.*Z1 + Z2)**2)**4 
		end select
	endif

	if( k1.eq.100 .and. k2.eq.301 ) then
		select case(lambda)
			case(0)
				RadialIntType1 = (-8.*Sqrt(6.)*Z1**1.5*Z2**3.5*(-81.*q**4 + 54.*q**2*(Z1 + Z2)*(3.*Z1 + Z2) + (9.*Z1 - Z2)*(3.*Z1 + Z2)**3))/(9.*q**2 + (3.*Z1 + Z2)**2)**4
			case(1)
				RadialIntType1 = (-32.*Sqrt(6.)*q*Z1**1.5*Z2**3.5*((9.*Z1 - 2.*Z2)*(3.*Z1 + Z2)**2 + 9.*q**2*(9.*Z1 + 4*Z2)))/(9.*q**2 + (3*Z1 + Z2)**2)**4
			case(2)
				RadialIntType1 = (-288.*Sqrt(6.)*q**2*Z1**1.5*Z2**3.5*(9.*(q**2 + Z1**2) - Z2**2))/(9.*q**2 + (3.*Z1 + Z2)**2)**4
		
		end select
	endif

	if( k1.eq.100 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType1 = (-32.*Sqrt(1.2)*Z1**1.5*Z2**4.5*(3.*Z1 + Z2)*(-9.*q**2 + (3.*Z1 + Z2)**2))/(9.*q**2 + (3.*Z1 + Z2)**2)**4
			case(1)
				RadialIntType1 = (32.*Sqrt(1.2)*q*Z1**1.5*Z2**4.5*(9.*q**2 - 5.*(3.*Z1 + Z2)**2))/(9.*q**2 + (3.*Z1 + Z2)**2)**4
			case(2)
				RadialIntType1 = (-576.*Sqrt(1.2)*q**2*Z1**1.5*Z2**4.5*(3.*Z1 + Z2))/(9.*q**2 + (3.*Z1 + Z2)**2)**4
			case(3)
				RadialIntType1 = (-1728.*Sqrt(1.2)*q**3*Z1**1.5*Z2**4.5)/(9.*q**2 + (3.*Z1 + Z2)**2)**4
		end select
	endif

! �������� �� 2s

	if( k1.eq.200 .and. k2.eq.200 ) then
		select case(lambda)
			case(0)
				RadialIntType1 = (-4.*Z1**1.5*Z2**2.5*(-8.*q**2*(5*Z1 - Z2)*Z2*(Z1 + Z2) + 16.*q**4*(4.*Z1 + 3.*Z2) -  &
							      (Z1 + Z2)**3*(4.*Z1**2 - 7.*Z1*Z2 + Z2**2)))/(4.*q**2 + (Z1 + Z2)**2)**4
			case(1)
				RadialIntType1 = (-16.*q*Z1**1.5*Z2**2.5*(16.*q**4 - 8.*q**2*Z1*(Z1 + 2.*Z2) - (Z1 + Z2)**2*(3.*Z1**2 - 6.*Z1*Z2 + Z2**2)))/ &
								 (4.*q**2 + (Z1 + Z2)**2)**4
			 
		end select
	endif

	if( k1.eq.200 .and. k2.eq.201 ) then
		select case(lambda)
			case(0)
				RadialIntType1 = (4.*Z1**1.5*Z2**3.5*(16.*q**4 + 3.*(3.*Z1 - Z2)*(Z1 + Z2)**3 - 8.*q**2*(Z1 + Z2)*(7.*Z1 + Z2)))/	&
								 (Sqrt(3.)*(4.*q**2 + (Z1 + Z2)**2)**4)  
			case(1)
				RadialIntType1 = (-32.*q*Z1**1.5*Z2**3.5*(-((4.*Z1 - Z2)*(Z1 + Z2)**2) + 4.*q**2*(2.*Z1 + Z2)))/ &
								 (Sqrt(3.)*(4.*q**2 + (Z1 + Z2)**2)**4)
			case(2)
				RadialIntType1 = (-64.*q**2*Z1**1.5*Z2**3.5*(4.*q**2 - 5.*Z1**2 - 4.*Z1*Z2 + Z2**2))/(Sqrt(3.)*(4.*q**2 + (Z1 + Z2)**2)**4)
				 
		end select
	endif


   if( k1.eq.200 .and. k2.eq.300 ) then
		select case(lambda)
			case(0)
				RadialIntType1 =   (32.*Sqrt(6.)*Z1**1.5*Z2**2.5*(-15552.*q**6*(27.*Z1 + 19.*Z2) - 	&
							       432.*q**4*(243.*Z1**3 - 603.*Z1**2*Z2 - 696.*Z1*Z2**2 - 28.*Z2**3) + 	&
							       (3.*Z1 + 2.*Z2)**4*(81.*Z1**3 - 189.*Z1**2*Z2 + 72.*Z1*Z2**2 - 4.*Z2**3) + &
							       12.*q**2*(3.*Z1 + 2.*Z2)**2*(243.*Z1**3 + 531.*Z1**2*Z2 - 1056.*Z1*Z2**2 + 92.*Z2**3)))/ &
								   (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
			case(1)
				RadialIntType1 =  (-96.*Sqrt(6.)*q*Z1**1.5*Z2**2.5*(139968.*q**6 - 1296.*q**4*(27*Z1**2 + 124.*Z1*Z2 + 28.*Z2**2) - & 
						          (3.*Z1 + 2.*Z2)**3*(243.*Z1**3 - 690.*Z1**2*Z2 + 356.*Z1*Z2**2 - 24.*Z2**3) -   &
								  36.*q**2*(3.*Z1 + 2.*Z2)*(405.*Z1**3 - 102.*Z1**2*Z2 - 740.*Z1*Z2**2 + 56.*Z2**3)))/	 &
								  (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
		end select
	endif


	if( k1.eq.200 .and. k2.eq.301 ) then
		select case(lambda)
			case(0)
				RadialIntType1 =   (64.*Sqrt(3.)*Z1**1.5*Z2**3.5*(46656.*q**6 + (3.*Z1 - 2.*Z2)*(27.*Z1 - 2.*Z2)*(3.*Z1 + 2.*Z2)**4 - 	&
								   180.*q**2*(3.*Z1 + 2.*Z2)**2*(9.*Z1**2 - 36.*Z1*Z2 + 4.*Z2**2) - 	 &
								   1296.*q**4*(117.*Z1**2 + 132.*Z1*Z2 + 20.*Z2**2)))/(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5  	  
			case(1)
				RadialIntType1 = (2048.*Sqrt(3.)*q*Z1**1.5*Z2**3.5*(-648.*q**4*(9.*Z1 + 4.*Z2) +  &
								 18.*q**2*(3.*Z1 + 2.*Z2)*(27.*Z1**2 + 66.*Z1*Z2 - 4.*Z2**2) + 	&
							     (3.*Z1 + 2.*Z2)**3*(27.*Z1**2 - 24.*Z1*Z2 + 2.*Z2**2)))/(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
			case(2)
				RadialIntType1 = (-9216.*Sqrt(3.)*q**2*Z1**1.5*Z2**3.5*(1296.*q**4 - 1296.*q**2*Z1*(Z1 + Z2) - 	&
							       (3.*Z1 + 2.*Z2)**2*(45.*Z1**2 - 48.*Z1*Z2 + 4.*Z2**2)))/(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
			
		end select
	endif


	if( k1.eq.200 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType1 =  (1024.*Sqrt(0.6)*Z1**1.5*Z2**4.5*(1296.*q**4*(3.*Z1 + Z2) - 540.*q**2*Z1*(3.*Z1 + 2.*Z2)**2 + & 
								  (6.*Z1 - Z2)*(3.*Z1 + 2.*Z2)**4))/(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5 	  
			case(1)
				RadialIntType1 =  (1024.*Sqrt(0.6)*q*Z1**1.5*Z2**4.5*(1296.*q**4 + 5.*(15.*Z1 - 2.*Z2)*(3.*Z1 + 2.*Z2)**3 -  &
								   72.*q**2*(3.*Z1 + 2.*Z2)*(33.*Z1 + 4.*Z2)))/(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
			case(2)
				RadialIntType1 =  (-73728.*Sqrt(0.6)*q**2*Z1**1.5*Z2**4.5*(36.*q**2*(3.*Z1 + Z2) - (9.*Z1 - Z2)*(3.*Z1 + 2.*Z2)**2))/	&
								  (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
			case(3)
				RadialIntType1 =  (-221184.*Sqrt(0.6)*q**3*Z1**1.5*Z2**4.5*(36.*q**2 - 63.*Z1**2 - 36.*Z1*Z2 + 4.*Z2**2))/ &
								  (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
		end select
	endif


! �������� �� 2p
	if( k1.eq.201 .and. k2.eq.201 ) then
		select case(lambda)
			case(0)
				RadialIntType1 =  (-16.*Z1**2.5*Z2**3.5*(Z1 + Z2)*(-4.*q**2 + (Z1 + Z2)**2))/(4.*q**2 + (Z1 + Z2)**2)**4
			case(1)
				RadialIntType1 =  (32.*q*Z1**2.5*Z2**3.5*(4.*q**2 - 5.*(Z1 + Z2)**2))/(3.*(4.*q**2 + (Z1 + Z2)**2)**4)
			case(2)
				RadialIntType1 =  (-128.*q**2*Z1**2.5*Z2**3.5*(Z1 + Z2))/(4.*q**2 + (Z1 + Z2)**2)**4
			case(3)
				RadialIntType1 =  (-256.*q**3.*Z1**2.5*Z2**3.5)/(4.*q**2 + (Z1 + Z2)**2)**4
		end select
	endif

	if( k1.eq.201 .and. k2.eq.300 ) then
		select case(lambda)
			case(0)
				RadialIntType1 =    (-144.*Sqrt(2.)*(Z1*Z2)**2.5*(-46656.*q**6 + 60.*q**2*(3.*Z1 + 2.*Z2)**2*(27.*Z1**2 + 36.*Z1*Z2 - 52.*Z2**2) + 	 &
							        (3.*Z1 + 2.*Z2)**4*(27.*Z1**2 - 44.*Z1*Z2 + 12.*Z2**2) + 1296.*q**4*(9.*Z1**2 + 92.*Z1*Z2 + 68.*Z2**2)))/		 &
								    (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5 
			case(1)
				RadialIntType1 =    (-384.*Sqrt(2.)*q*(Z1*Z2)**2.5*(1296.*q**4*(27.*Z1 + 38.*Z2) + 	&
									72.*q**2*(3.*Z1 + 2.*Z2)*(81.*Z1**2 - 12.*Z1*Z2 - 116.*Z2**2) + &
								    (3.*Z1 + 2.*Z2)**3*(81.*Z1**2 - 192.*Z1*Z2 + 76.*Z2**2)))/(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
			case(2)
				RadialIntType1 =   (-6912.*Sqrt(2.)*q**2*(Z1*Z2)**2.5*(3888.*q**4 + (9.*Z1 - 22.*Z2)*(3.*Z1 - 2.*Z2)*(3.*Z1 + 2.*Z2)**2 + &
							       216.*q**2*(9.*Z1**2 - 8.*Z1*Z2 - 12.*Z2**2)))/(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
		end select
	endif

	if( k1.eq.201 .and. k2.eq.301 ) then
		select case(lambda)
			case(0)
				RadialIntType1 = (-768.*Z1**2.5*Z2**3.5*(720.*q**2*Z2*(3.*Z1 + 2.*Z2)**2 + (9.*Z1 - 4.*Z2)*(3.*Z1 + 2.*Z2)**4 - &
						         1296.*q**4*(9.*Z1 + 8.*Z2)))/(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5   
			case(1)
				RadialIntType1 = (4608.*q*Z1**2.5*Z2**3.5*(1296.*q**4 - 5.*(3.*Z1 - 2.*Z2)*(3.*Z1 + 2.*Z2)**3 -   &
						         144.*q**2*(3.*Z1 + 2.*Z2)*(3.*Z1 + 5.*Z2)))/(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5   
			case(2)
				RadialIntType1 = (-55296.*q**2*Z1**2.5*Z2**3.5*((9.*Z1 - 8.*Z2)*(3.*Z1 + 2.*Z2)**2 + 36.*q**2*(9.*Z1 + 8.*Z2)))/	&
								 (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
			case(3)
				RadialIntType1 = (-331776.*q**3*Z1**2.5*Z2**3.5*(108.*q**2 + (9.*Z1 - 10.*Z2)*(3.*Z1 + 2.*Z2)))/(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
		end select
	endif

	if( k1.eq.201 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType1 = (-1536.*Z1**2.5*Z2**4.5*(1296.*q**4 - 360.*q**2*(3.*Z1 + 2.*Z2)**2 + 5.*(3.*Z1 + 2.*Z2)**4))/ &
								 (Sqrt(5.)*(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5)  
			case(1)
				RadialIntType1 = (18432.*q*Z1**2.5*Z2**4.5*(3.*Z1 + 2.*Z2)*(108.*q**2 - 5.*(3.*Z1 + 2.*Z2)**2))/ &
								 (Sqrt(5.)*(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5)
			case(2)
				RadialIntType1 = (110592.*q**2*Z1**2.5*Z2**4.5*(36.*q**2 - 7.*(3.*Z1 + 2.*Z2)**2))/(Sqrt(5.)*(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5)
			case(3)
				RadialIntType1 = (-5308416.*q**3*Z1**2.5*Z2**4.5*(3.*Z1 + 2.*Z2))/(Sqrt(5.)*(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5)
			case(4)
				RadialIntType1 = (-31850496.*q**4*Z1**2.5*Z2**4.5)/(Sqrt(5.)*(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5)
		end select
	endif



! �������� �� 3s
	if( k1.eq.300 .and. k2.eq.300 ) then
		select case(lambda)
			case(0)
				RadialIntType1 = (-8.*Z1**1.5*Z2**2.5*(27.*q**8*(27.*Z1 + 19.*Z2) -   &
								 12.*q**6*(9.*Z1**3 + 71.*Z1**2*Z2 + 51.*Z1*Z2**2 - 3.*Z2**3) + 	&
								 ((Z1 + Z2)**5*(27.*Z1**4 - 172.*Z1**3*Z2 + 218.*Z1**2*Z2**2 - 60.*Z1*Z2**3 + 3.*Z2**4))/243. - 	&
								 (4.*q**2*(Z1 + Z2)**3*(9.*Z1**4 - 104.*Z1**3*Z2 + 210.*Z1**2*Z2**2 - 72.*Z1*Z2**3 + 5.*Z2**4))/27. -    &
								 2.*q**4*(Z1 + Z2)*(21.*Z1**4 - 28.*Z1**3*Z2 - 146.*Z1**2*Z2**2 - 12.*Z1*Z2**3 + 5.*Z2**4)))/		&
								 (19683.*(q**2 + (Z1 + Z2)**2/9.)**6)
			case(1)
				RadialIntType1 = (-8.*q*Z1**1.5*Z2**2.5*(59049.*q**8 - 8748.*q**6*(5.*Z1**2 + 10.*Z1*Z2 + Z2**2) + 	  &
						         36.*q**2*(Z1 + Z2)**2*(9.*Z1**4 + 52.*Z1**3*Z2 - 230.*Z1**2*Z2**2 + 60.*Z1*Z2**3 - 3.*Z2**4) -   &
								 486.*q**4*(15.*Z1**4 - 52.*Z1**3*Z2 - 126.*Z1**2*Z2**2 - 36.*Z1*Z2**3 + 7.*Z2**4) + 	 &
								 (Z1 + Z2)**4*(57.*Z1**4 - 380.*Z1**3*Z2 + 518.*Z1**2*Z2**2 - 156.*Z1*Z2**3 + 9.*Z2**4)))/	 &
								 (3.*(9.*q**2 + (Z1 + Z2)**2)**6)
		end select
	endif

	if( k1.eq.300 .and. k2.eq.301 ) then
		select case(lambda)
			case(0)
				RadialIntType1 =  (8.*Sqrt(2.)*Z1**1.5*Z2**3.5*(6561.*q**8 - 2916.*q**6*(Z1 + Z2)*(8.*Z1 + Z2) +   &
							      162.*q**4*(Z1 + Z2)*(21.*Z1**3 + 77.*Z1**2*Z2 + 11.*Z1*Z2**2 - 5.*Z2**3) + 	 &
						          12.*q**2*(Z1 + Z2)**3*(42.*Z1**3 - 119.*Z1**2*Z2 + 36.*Z1*Z2**2 - 3.*Z2**3) -    &
						          (Z1 + Z2)**5*(19.*Z1**3 - 43.*Z1**2*Z2 + 17.*Z1*Z2**2 - Z2**3)))/(3.*(9.*q**2 + (Z1 + Z2)**2)**6)
			case(1)
				RadialIntType1 =    (-32.*Sqrt(2.)*q*Z1**1.5*Z2**3.5*(9.*q**6*(9.*Z1 + 4.*Z2) - 	 &
							        (7.*q**2*Z1*(Z1 + Z2)**2*(Z1**2 - 6.*Z1*Z2 + Z2**2))/3. -   &
								    3.*q**4*(15.*Z1**3 + 32.*Z1**2*Z2 + 11.*Z1*Z2**2 - 2.*Z2**3) +  &
							        ((Z1 + Z2)**4*(33.*Z1**3 - 76.*Z1**2*Z2 + 29.*Z1*Z2**2 - 2.*Z2**3))/81.))/ &
								    (19683.*(q**2 + (Z1 + Z2)**2/9.)**6)
			case(2)
				RadialIntType1 =    (-32.*Sqrt(2.)*q**2*Z1**1.5*Z2**3.5*(2187.*q**6 - 243.*q**4*(13.*Z1 - Z2)*(Z1 + Z2) + 	&
								    (3.*Z1 - Z2)*(Z1 + Z2)**3*(17.*Z1**2 - 36.*Z1*Z2 + 3.*Z2**2) + 	 &
								    27.*q**2*(Z1 + Z2)*(3.*Z1**3 + 37.*Z1**2*Z2 + Z1*Z2**2 - Z2**3)))/(9.*q**2 + (Z1 + Z2)**2)**6
					
		end select
	endif
	
	if( k1.eq.300 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType1 =   (32.*Sqrt(0.4)*Z1**1.5*Z2**4.5*(729.*q**6*(3.*Z1 + Z2) +  &
							       3.*q**2*(Z1 + Z2)**3*(167.*Z1**2 - 36.*Z1*Z2 - 3.*Z2**2) -  &
							       81.*q**4*(Z1 + Z2)*(37.*Z1**2 + 16.*Z1*Z2 - Z2**2) - (Z1 + Z2)**5*(11.*Z1**2 - 8.*Z1*Z2 + Z2**2)))/ &
								   (3.*(9.*q**2 + (Z1 + Z2)**2)**6) 
			case(1)
				RadialIntType1 =   (32.*Sqrt(0.4)*q*Z1**1.5*Z2**4.5*(729.*q**6 + 27.*q**2*(Z1 + Z2)**2*(61.*Z1**2 + 2.*Z1*Z2 - 3.*Z2**2) - &
							       5.*(Z1 + Z2)**4*(17.*Z1**2 - 10.*Z1*Z2 + Z2**2) - 243.*q**4*(17.*Z1**2 + 14.*Z1*Z2 + Z2**2)))/			  &
								   (3.*(9.*q**2 + (Z1 + Z2)**2)**6)  
			case(2)
				RadialIntType1 =   (-64.*Sqrt(0.4)*q**2*Z1**1.5*Z2**4.5*(9.*q**4*(3.*Z1 + Z2) -  &
							       2.*q**2*(Z1 + Z2)*(13.*Z1**2 + 4.*Z1*Z2 - Z2**2) + ((Z1 + Z2)**3*(73.*Z1**2 - 36.*Z1*Z2 + 3.*Z2**2))/27.	 &
							       ))/(19683.*(q**2 + (Z1 + Z2)**2/9.)**6)
			case(3)
				RadialIntType1 =   (-576.*Sqrt(0.4)*q**3*Z1**1.5*Z2**4.5* &
								   (81.*q**4 - 6.*q**2*(29.*Z1**2 + 18.*Z1*Z2 - 3.*Z2**2) + (Z1 + Z2)**2*(33.*Z1**2 - 14.*Z1*Z2 + Z2**2)))/ &
								   (9.*q**2 + (Z1 + Z2)**2)**6
		end select
	endif

! �������� �� 3p
	if( k1.eq.301 .and. k2.eq.301 ) then
		select case(lambda)
			case(0)
				RadialIntType1 =   (64.*Z1**2.5*Z2**3.5*(3.*q**6*(9.*Z1 + 8.*Z2) + ((Z1 + Z2)**5*(9.*Z1**2 - 17.*Z1*Z2 + 4.*Z2**2))/243. - 	&
							       q**4*(Z1 + Z2)*(7.*Z1**2 + 21.*Z1*Z2 + 4.*Z2**2) -  &
							       (q**2*(Z1 + Z2)**3*(21.*Z1**2 - 63.*Z1*Z2 + 16.*Z2**2))/27.))/(19683.*(q**2 + (Z1 + Z2)**2/9.)**6)
			case(1)
				RadialIntType1 =   (128.*q*Z1**2.5*Z2**3.5*(9.*q**6 - 3.*q**4*(2.*Z1 + Z2)*(2.*Z1 + 3.*Z2) +  &
							       (5.*(Z1 + Z2)**4*(2.*Z1**2 - 4.*Z1*Z2 + Z2**2))/81. -  &
							       (q**2*(Z1 + Z2)**2*(3.*Z1**2 - 34.*Z1*Z2 + 5.*Z2**2))/9.))/(19683.*(q**2 + (Z1 + Z2)**2/9.)**6)
			case(2)
				RadialIntType1 =   (-128.*q**2*Z1**2.5*Z2**3.5*(-54.*q**2*Z1*(Z1 + Z2)*(Z1 + 5.*Z2) + 81.*q**4*(9.*Z1 + 8.*Z2) - &
							       (Z1 + Z2)**3*(15.*Z1**2 - 33.*Z1*Z2 + 8.*Z2**2)))/(9.*q**2 + (Z1 + Z2)**2)**6 
			
			case(3)
				RadialIntType1 =   (-768.*q**3*Z1**2.5*Z2**3.5*(243.*q**4 - 18.*q**2*(3.*Z1**2 + 6.*Z1*Z2 + Z2**2) - &
							       (Z1 + Z2)**2*(9.*Z1**2 - 22.*Z1*Z2 + 5.*Z2**2)))/(9.*q**2 + (Z1 + Z2)**2)**6
			
		end select
	endif

	if( k1.eq.301 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType1 =   (128.*Z1**2.5*Z2**4.5*(-729.*q**6 - 45.*q**2*(9.*Z1 - Z2)*(Z1 + Z2)**3 + 5.*(2.*Z1 - Z2)*(Z1 + Z2)**5 + &
							       243.*q**4*(Z1 + Z2)*(8.*Z1 + 3.*Z2)))/(9.*Sqrt(5.)*(9.*q**2 + (Z1 + Z2)**2)**6)
			case(1)
				RadialIntType1 =   (128.*q*Z1**2.5*Z2**4.5*(5.*(5.*Z1 - 2.*Z2)*(Z1 + Z2)**4 + 243.*q**4*(3.*Z1 + 2.*Z2) - &
								    18.*q**2*(Z1 + Z2)**2*(23.*Z1 + 2.*Z2)))/(3.*Sqrt(5.)*(9.*q**2 + (Z1 + Z2)**2)**6)
			case(2)
				RadialIntType1 =   (256.*q**2*Z1**2.5*Z2**4.5*(3.*q**4 + (7.*(3.*Z1 - Z2)*(Z1 + Z2)**3)/27. - 2.*q**2*(Z1 + Z2)*(3.*Z1 + Z2)))/	 &
								   (19683.*Sqrt(5.)*(q**2 + (Z1 + Z2)**2/9.)**6)
			
			case(3)
				RadialIntType1 =   (-3072.*q**3*Z1**2.5*Z2**4.5*(-((7.*Z1 - 2.*Z2)*(Z1 + Z2)**2) + 9.*q**2*(3.*Z1 + 2*Z2)))/	  &
								   (Sqrt(5.)*(9*q**2 + (Z1 + Z2)**2)**6)
			case(4)
				RadialIntType1 =    (-2048.*q**4*Z1**2.5*Z2**4.5* &
								    (3.*q**2 + ((-4.*Z1 + Z2)*(Z1 + Z2))/3.))/ &
								    (19683.*Sqrt(5.)*(q**2 + (Z1 + Z2)**2/9.)**6)

		end select
	endif

	

! �������� �� 3d
	
	if( k1.eq.302 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType1 =  (-128.*Z1**3.5*Z2**4.5*(Z1 + Z2)*		&
						          (81.*q**4 - 30.*q**2*(Z1 + Z2)**2 + (Z1 + Z2)**4))/  &
							      (3.*(9.*q**2 + (Z1 + Z2)**2)**6)  
			case(1)
				RadialIntType1 =  (-128.*q*Z1**3.5*Z2**4.5*	   &
								  (243.*q**4 - 378.*q**2*(Z1 + Z2)**2 + 35.*(Z1 + Z2)**4))/	&
							      (15.*(9.*q**2 + (Z1 + Z2)**2)**6) 
			case(2)
				RadialIntType1 =  (-1024.*q**2*Z1**3.5*Z2**4.5*(Z1 + Z2)*   &
							      (-27.*q**2 + 7.*(Z1 + Z2)**2))/			   &
								  (5.*(9.*q**2 + (Z1 + Z2)**2)**6) 			
			case(3)
				RadialIntType1 =  (27648.*q**3*Z1**3.5*Z2**4.5*(q**2 - (Z1 + Z2)**2))/	  &
								  (5.*(9.*q**2 + (Z1 + Z2)**2)**6) 

			case(4)
				RadialIntType1 =  (-18432.*q**4*Z1**3.5*Z2**4.5*(Z1 + Z2))/	 &
								  (9.*q**2 + (Z1 + Z2)**2)**6

			case(5)
				RadialIntType1 =  (-55296.*q**5*Z1**3.5*Z2**4.5)/(9.*q**2 + (Z1 + Z2)**2)**6

		end select
	endif




!=============================================================================================================================
END FUNCTION RadialIntType1


REAL FUNCTION RadialIntType2(Z1, n1, l1, Z2, n2, l2, q, lambda)
!=============================================================================================================================
! ���������� �������� ����
!
!  \int R_{n_1,l_1} j_\lambda(qr) \left( \frac{d R_{n_2,l_2}}{dr} + \frac{l_2+1}{r}R_{n_2,l_2} \right) r^2 dr
!
!
! ���������� ������ ��� ������ 
! n1<=n2 && l1<=l2
!
! ��� ������� ������ ������������ ������� ������������ � ����� ��������� ��������,
! ��� �������� � ������� ������.
!
!
! lambda �� 0 �� 5
!=============================================================================================================================
real, intent(in) :: Z1, Z2, q
integer, intent(in) :: n1, l1, n2, l2, lambda
integer :: k1, k2

k1 = 100*n1 + l1
k2 = 100*n2 + l2

if(k1.gt.k2) then
	print*, 'ERROR in A_ddx'
	STOP
endif

k1 = 100*n1 + l1
k2 = 100*n2 + l2

if(k1.gt.k2) print*, 'ERROR' 

! �������� �� 1s
	if( k1.eq.100 .and. k2.eq.100 ) then
		select case(lambda)
			case(0)
				RadialIntType2 = (4.*(Z1*Z2)**1.5*(q**2 + Z1**2 - Z2**2))/(q**2 + (Z1 + Z2)**2)**2  
		end select
	endif

	if( k1.eq.100 .and. k2.eq.200 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =   (4.*Sqrt(2.)*(Z1*Z2)**1.5*	&
								   (16.*(q**2 + Z1**2)**2 - 16.*Z1*(q**2 + Z1**2)*Z2 - 	&
								    24.*(q**2 + Z1**2)*Z2**2 - 4.*Z1*Z2**3 + Z2**4))/	&
								   (4.*q**2 + (2.*Z1 + Z2)**2)**3
		end select
	endif

	if( k1.eq.100 .and. k2.eq.201 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =   (16.*Sqrt(2./3.)*Z1**1.5*Z2**2.5*   &
							       (3.*Z1*(2.*Z1 + Z2)**2 + 4.*q**2*(3.*Z1 + 2.*Z2)))/ &
								   (4.*q**2 + (2.*Z1 + Z2)**2)**3  
			case(1)
				RadialIntType2 =   (16.*Sqrt(2./3.)*q*Z1**1.5*Z2**2.5*	&
							       (12.*q**2 + (6.*Z1 - Z2)*(2.*Z1 + Z2)))/  &
								   (4.*q**2 + (2.*Z1 + Z2)**2)**3
		end select
	endif

	if( k1.eq.100 .and. k2.eq.300 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =     (4.*Sqrt(3.)*(Z1*Z2)**1.5*									  &
							         (729.*(q**2 + Z1**2)**3 - 972.*Z1*(q**2 + Z1**2)**2*Z2 - 	   &
								     81.*(q**2 + Z1**2)*(15.*q**2 + 11.*Z1**2)*Z2**2 + 				&
							         9.*(15.*q**2 + 11.*Z1**2)*Z2**4 + 12.*Z1*Z2**5 - Z2**6))/		 &
								     (9.*q**2 + (3.*Z1 + Z2)**2)**4
		end select
	endif

	if( k1.eq.100 .and. k2.eq.301 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =   (48.*Sqrt(6.)*Z1**1.5*Z2**2.5*(9.*(q**2 + Z1**2) - Z2**2)*	 &
							       (Z1*(3.*Z1 + Z2)**2 + q**2*(9.*Z1 + 6.*Z2)))/				  &
								   (9.*q**2 + (3.*Z1 + Z2)**2)**4
			case(1)
				RadialIntType2 =   (16.*Sqrt(6.)*q*Z1**1.5*Z2**2.5*			  &
								   (243.*(q**2 + Z1**2)**2 - 72.*(q**2 + Z1**2)*Z2**2 -  &
							       12.*Z1*Z2**3 + Z2**4))/(9.*q**2 + (3.*Z1 + Z2)**2)**4  
		end select
	endif

	if( k1.eq.100 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType2 = (8.*Sqrt(1.2)*Z1**1.5*Z2**3.5*					   &
							     (-135.*q**4 + (3.*Z1 + Z2)**3*(15.*Z1 + Z2) + 		 &
							     6.*q**2*(3.*Z1 + Z2)*(15.*Z1 + 11.*Z2)))/			 &
							     (9.*q**2 + (3.*Z1 + Z2)**2)**4 
			case(1)
				RadialIntType2 = (96.*Sqrt(1.2)*q*Z1**1.5*Z2**3.5*					  &
								 (5.*Z1*(3.*Z1 + Z2)**2 + 9.*q**2*(5.*Z1 + 2.*Z2)))/  &
							     (9.*q**2 + (3.*Z1 + Z2)**2)**4						  
			case(2)
				RadialIntType2 =  (96.*Sqrt(1.2)*q**2*Z1**1.5*Z2**3.5*		  &
							      (45.*q**2 + (15.*Z1 - Z2)*(3.*Z1 + Z2)))/	   &
								  (9.*q**2 + (3.*Z1 + Z2)**2)**4
		end select
	endif

! �������� �� 2s

	if( k1.eq.200 .and. k2.eq.200 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =     (-2.*(Z1*Z2)**1.5*(-64.*q**6 - 		  &
							         16.*q**4*(Z1**2 - 8.*Z1*Z2 - 5.*Z2**2) + 	&
							         4.*q**2*(Z1 - 5.*Z2)*(Z1 + Z2)*			&
							         (Z1**2 + 4.*Z1*Z2 - Z2**2) + 				 &
							         (Z1 - Z2)*(Z1 + Z2)**3*(Z1**2 - 10.*Z1*Z2 + Z2**2)))/  &
									 (4.*q**2 + (Z1 + Z2)**2)**4 
		end select
	endif

	if( k1.eq.200 .and. k2.eq.201 ) then
		select case(lambda)
			case(0)
				RadialIntType2 = (8.*Z1**1.5*Z2**2.5*										 &
								 (-8.*q**2*(2.*Z1 - Z2)*Z2*(Z1 + Z2) - 						  &
							     3.*Z1*(Z1 - Z2)*(Z1 + Z2)**3 + 16.*q**4*(3.*Z1 + 2.*Z2)))/	   &
								 (Sqrt(3.)*(4.*q**2 + (Z1 + Z2)**2)**4)
			case(1)
				RadialIntType2 = (8.*q*Z1**1.5*Z2**2.5*										 &
							     (48.*q**4 - (Z1 - Z2)*(9.*Z1 - Z2)*(Z1 + Z2)**2 - 			  &
							     8.*q**2*(3.*Z1**2 + 4.*Z1*Z2 - Z2**2)))/					   &
								 (Sqrt(3.)*(4.*q**2 + (Z1 + Z2)**2)**4)
		end select
	endif


   if( k1.eq.200 .and. k2.eq.300 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =      (-4.*Sqrt(6.)*(Z1*Z2)**1.5*			&
								      (-1679616.*q**8 + 					 &
								      15552.*q**4*Z1*Z2*(63.*Z1**2 - 156.*Z1*Z2 - 164.*Z2**2) -  &
								      93312.*q**6*(9.*Z1**2 - 42.*Z1*Z2 - 28.*Z2**2) + 			 &
								      72.*q**2*(3.*Z1 + 2.*Z2)**2*								 &
								      (81.*Z1**4 - 486.*Z1**3*Z2 - 1008.*Z1**2*Z2**2 + 			 &
								      1320.*Z1*Z2**3 - 112.*Z2**4) + 							 &
								      (3.*Z1 + 2.*Z2)**4*										 &
								      (81.*Z1**4 - 972.*Z1**3*Z2 + 1512.*Z1**2*Z2**2 - 			 &
								      432.*Z1*Z2**3 + 16.*Z2**4)))/								 &
									  (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5  
		end select
	endif


	if( k1.eq.200 .and. k2.eq.301 ) then
		select case(lambda)
			case(0)																				 
				RadialIntType2 =   	(-384.*Sqrt(3.)*Z1**1.5*Z2**2.5*							 &
								    (-15552.*q**6*(3.*Z1 + 2.*Z2) - 							 &
								    1296.*q**4*Z1*(9.*Z1**2 - 18.*Z1*Z2 - 20.*Z2**2) + 			 &
								    Z1*(3.*Z1 + 2.*Z2)**4*(9.*Z1**2 - 18.*Z1*Z2 + 4.*Z2**2) + 	 &
								    12.*q**2*(3.*Z1 + 2.*Z2)**2*								 &
								    (27.*Z1**3 + 54.*Z1**2*Z2 - 84.*Z1*Z2**2 + 8.*Z2**3)))/		 &
								    (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5  
			case(1)
				RadialIntType2 =   (128.*Sqrt(3.)*q*Z1**1.5*Z2**2.5*		  &
								   (139968.*q**6 - 1296.*q**4*			   &
								   (27.*Z1**2 + 108.*Z1*Z2 + 20.*Z2**2) - 		&
								   (3.*Z1 + 2.*Z2)**3*						 &
								   (243.*Z1**3 - 594.*Z1**2*Z2 + 204.*Z1*Z2**2 - 8.*Z2**3)  &
								    - 36.*q**2*(3.*Z1 + 2.*Z2)*							  &
								   (405.*Z1**3 - 54.*Z1**2*Z2 - 564.*Z1*Z2**2 + 56.*Z2**3)))   &
								   /(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
		end select
	endif


	if( k1.eq.200 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =      (-64.*Sqrt(0.6)*Z1**1.5*Z2**3.5*			    &
									  (77760.*q**6 + (3.*Z1 + 2.*Z2)**4*		     &
									  (135.*Z1**2 - 36.*Z1*Z2 - 4.*Z2**2) - 		  &
									  60.*q**2*(3.*Z1 + 2.*Z2)**2*					   &
									  (45.*Z1**2 - 84.*Z1*Z2 + 20.*Z2**2) - 		    &
									  432.*q**4*(585.*Z1**2 + 564.*Z1*Z2 + 68.*Z2**2)))/ &
									  (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
			case(1)
				RadialIntType2 =       (6144.*Sqrt(0.6)*q*Z1**1.5*Z2**3.5*						   &
								       (-5.*Z1*(3.*Z1 - Z2)*(3.*Z1 + 2.*Z2)**3 + 					&
								       648.*q**4*(5.*Z1 + 2.*Z2) - 									  &
								       18.*q**2*(3.*Z1 + 2.*Z2)*(15.*Z1**2 + 22.*Z1*Z2 - 4.*Z2**2)))/  &
								       (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
			case(2)
				RadialIntType2 =  	    (3072.*Sqrt(0.6)*q**2*Z1**1.5*Z2**3.5*						  &
									    (6480.*q**4 - 144.*q**2*(45.*Z1**2 + 33.*Z1*Z2 - 4.*Z2**2) -   &
								        (3.*Z1 + 2.*Z2)**2*(225.*Z1**2 - 96.*Z1*Z2 + 4.*Z2**2)))/		&
									    (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5								 
		end select
	endif


! �������� �� 2p
	if( k1.eq.201 .and. k2.eq.201 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =  (4.*(Z1*Z2)**2.5*(-16.*q**4 + (3.*Z1 - Z2)*(Z1 + Z2)**3 +    &
							      8.*q**2*(Z1 + Z2)*(Z1 + 3.*Z2)))/								&
								  (4.*q**2 + (Z1 + Z2)**2)**4
			case(1)
				RadialIntType2 =  (32.*q*(Z1*Z2)**2.5*											&
								  ((3.*Z1 - 2.*Z2)*(Z1 + Z2)**2 + 4.*q**2*(3.*Z1 + 4.*Z2)))/	 &
								  (3.*(4.*q**2 + (Z1 + Z2)**2)**4)
			case(2)
				RadialIntType2 =  (64.*q**2*(Z1*Z2)**2.5*(4.*q**2 + Z1**2 - Z2**2))/			  &
								  (4.*q**2 + (Z1 + Z2)**2)**4
		end select
	endif

	if( k1.eq.201 .and. k2.eq.300 ) then
		select case(lambda)
			case(0)
				RadialIntType2 = (24.*Sqrt(2.)*(192.*Z1**2.5*Z2**3.5*(3.*Z1 + 2.*Z2)*			 &
						         (-36.*q**2 + (3.*Z1 + 2.*Z2)**2)*								  &
							     (36.*q**2 + (3.*Z1 + 2.*Z2)**2) - 								   &
							     30.*(Z1*Z2)**2.5*(-12.*q**2 + (3.*Z1 + 2.*Z2)**2)*					&
							     (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**2 + 								 &
							     Z1**2.5*Z2**1.5*(3.*Z1 + 2.*Z2)*									  &
							     (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**3 - 								   &
							     64.*Z1**2.5*Z2**4.5*													&
							     (1296.*q**4 - 360.*q**2*(3.*Z1 + 2.*Z2)**2 + 							 &
							     5.*(3.*Z1 + 2.*Z2)**4)))/												  &
							     (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5   
			case(1)
				RadialIntType2 =  (144.*Sqrt(2.)*q*Z1**2.5*Z2**1.5*									&
							      (46656.*q**6 + 3888.*q**4*										 &
						          (9.*Z1**2 - 28.*Z1*Z2 - 44.*Z2**2) + 								  &
						          (3.*Z1 + 2.*Z2)**3*												   &
							      (27.*Z1**3 - 306.*Z1**2*Z2 + 516.*Z1*Z2**2 - 152.*Z2**3)				&
							      + 108.*q**2*(3.*Z1 + 2.*Z2)*											 &
							      (27.*Z1**3 - 186.*Z1**2*Z2 - 28.*Z1*Z2**2 + 200.*Z2**3)))				  &
							      /(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5 
		end select
	endif

	if( k1.eq.201 .and. k2.eq.301 ) then
		select case(lambda)
			case(0)	
				RadialIntType2 =     (192.*(Z1*Z2)**2.5*(-46656.*q**6 + 							 &
								     540.*q**2*(3.*Z1 - 2.*Z2)*(Z1 + 2.*Z2)*(3.*Z1 + 2.*Z2)**2 + 	  &
								     (3.*Z1 + 2.*Z2)**4*(27.*Z1**2 - 36.*Z1*Z2 + 4.*Z2**2) + 		   &
								     3888.*q**4*(3.*Z1**2 + 28.*Z1*Z2 + 20.*Z2**2)))/					&
								     (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
			case(1)
				RadialIntType2 =     (4608.*q*(Z1*Z2)**2.5*										 &
								     (1296.*q**4*(3.*Z1 + 4.*Z2) + 								  &
								     72.*q**2*(3.*Z1 + 2.*Z2)*(9.*Z1**2 - 10.*Z2**2) + 			   &
								     (3.*Z1 + 2.*Z2)**3*(9.*Z1**2 - 18.*Z1*Z2 + 4.*Z2**2)))/		&
									 (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
			case(2)
				RadialIntType2 = 	 (27648.*q**2*(Z1*Z2)**2.5*										 &
									 (81.*(4*q**2 + Z1**2)**2 - 108.*Z1*(4.*q**2 + Z1**2)*Z2 - 		  &
									 180.*(4*q**2 + Z1**2)*Z2**2 + 32.*Z2**4))/						   &
									 (36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5
		end select
	endif

	if( k1.eq.201 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =   (2304.*Z1**2.5*Z2**3.5*										   &
								   (240.*q**2*Z2*(3.*Z1 + 2.*Z2)**2 + 5.*Z1*(3.*Z1 + 2.*Z2)**4 - 	&
								   1296.*q**4*(5.*Z1 + 4.*Z2)))/									 &
								   (Sqrt(5.)*(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5)
			case(1)
				RadialIntType2 =   (-1536.*q*Z1**2.5*Z2**3.5*									   &
								   (6480.*q**4 - 5.*(15.*Z1 - 2.*Z2)*(3.*Z1 + 2.*Z2)**3 - 			&
								   144.*q**2*(3.*Z1 + 2.*Z2)*(15.*Z1 + 19.*Z2)))/					 &
								   (Sqrt(5.)*(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5)
			case(2)
				RadialIntType2 =   (55296.*q**2*Z1**2.5*Z2**3.5*								   &
								   ((15.*Z1 - 4.*Z2)*(3.*Z1 + 2.*Z2)**2 + 							&
								   108.*q**2*(5.*Z1 + 4.*Z2)))/										 &
								   (Sqrt(5.)*(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5)
			case(3)
				RadialIntType2 =   (995328.*q**3*Z1**2.5*Z2**3.5*								   &
								   (60.*q**2 + 15.*Z1**2 + 4.*Z1*Z2 - 4.*Z2**2))/					&
								   (Sqrt(5.)*(36.*q**2 + (3.*Z1 + 2.*Z2)**2)**5)
		end select
	endif



! �������� �� 3s
	if( k1.eq.300 .and. k2.eq.300 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =      (4.*(Z1*Z2)**1.5*(59049.*q**10 - 							 &
									  2187.*q**8*(Z1**2 + 72.*Z1*Z2 + 39.*Z2**2) + 				  &
									  (Z1 - Z2)*(Z1 + Z2)**5*									   &
									  (Z1**4 - 28.*Z1**3.*Z2 + 102.*Z1**2*Z2**2 - 					&
									  28.*Z1*Z2**3 + Z2**4) - 										 &
									  1458.*q**6*(3.*Z1**4 - 16.*Z1**3*Z2 - 114.*Z1**2*Z2**2 - 		  &
									  72.*Z1*Z2**3 + 7.*Z2**4) - 									   &
									  3.*q**2*(Z1 + Z2)**3*												&
									  (Z1**5 - 99.*Z1**4*Z2 + 906.*Z1**3*Z2**2 - 						 &
									  1606.*Z1**2*Z2**3 + 549.*Z1*Z2**4 - 39.*Z2**5) - 					  &
									  162.*q**4*(Z1 + Z2)*												   &
									  (3.*Z1**5 - 59.*Z1**4*Z2 + 38.*Z1**3*Z2**2 + 							&
									  274.*Z1**2*Z2**3 + 7.*Z1*Z2**4 - 7.*Z2**5)))/							 &
									  (3.*(9.*q**2 + (Z1 + Z2)**2)**6)										  
			case(1)
				RadialIntType2 = 0.0
		end select
	endif

	if( k1.eq.300 .and. k2.eq.301 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =   (16.*Sqrt(2.)*Z1**1.5*Z2**2.5*					 &
								   (81.*q**8*(3.*Z1 + 2.*Z2) + 						  &
								   (Z1*(Z1 - Z2)*(Z1 + Z2)**5*						   &
								   (3.*Z1**2 - 14.*Z1*Z2 + 3.*Z2**2))/81. - 			&
								   18.*q**6*(2.*Z1**3 + 14.*Z1**2*Z2 + 9.*Z1*Z2**2 - 	 &
								   Z2**3) - (2.*q**2*(Z1 + Z2)**3*						  &
								   (6.*Z1**4 - 60.*Z1**3*Z2 + 95.*Z1**2*Z2**2 - 		   &
								   36.*Z1*Z2**3 + 3.*Z2**4))/27. - 							&
								   2.*q**4*(7.*Z1**5 - 43.*Z1**3*Z2**2 - 37.*Z1**2*Z2**3 + 	 &
								   Z2**5)))/(19683.*(q**2 + (Z1 + Z2)**2/9.)**6)
			case(1)
				RadialIntType2 =  (16.*Sqrt(2.)*q*Z1**1.5*Z2**2.5*							   &
								  (19683.*q**8 - 1458.*q**6*(10.*Z1**2 + 18.*Z1*Z2 + Z2**2) + 	&
								  (Z1 - Z2)*(Z1 + Z2)**4*										 &
								  (19.*Z1**3 - 93.*Z1**2*Z2 + 27.*Z1*Z2**2 - Z2**3) + 			  &
								  54.*q**2*(Z1 + Z2)**2*										   &
								  (2.*Z1**4 + 10.*Z1**3*Z2 - 35.*Z1**2*Z2**2 + 						&
								  12.*Z1*Z2**3 - Z2**4) - 											 &
								  486.*q**4*(5.*Z1**4 - 14.*Z1**3*Z2 - 33.*Z1**2*Z2**2 - 			  &
								  8.*Z1*Z2**3 + 2.*Z2**4)))/										   &
								  (3.*(9.*q**2 + (Z1 + Z2)**2)**6)  									
		end select
	endif
	
	if( k1.eq.300 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =  (-8.*Sqrt(0.4)*Z1**1.5*Z2**3.5*							&
								  (32805.*q**8 - 2916.*q**6*								 &
								  (40.*Z1**2 + 39.*Z1*Z2 + 3.*Z2**2) + 						  &
								  486.*q**4*(Z1 + Z2)*										   &
								  (35.*Z1**3 + 79.*Z1**2*Z2 - 3.*Z1*Z2**2 - 7.*Z2**3) + 		&
								  36.*q**2*(Z1 + Z2)**3*										 &
								  (70.*Z1**3 - 87.*Z1**2*Z2 + 36.*Z1*Z2**2 - 7.*Z2**3) - 		  &
								  (Z1 + Z2)**5*(95.*Z1**3 - 127.*Z1**2*Z2 + 21.*Z1*Z2**2 + 		   &
								  3.*Z2**3)))/(9.*(9.*q**2 + (Z1 + Z2)**2)**6)  					
			case(1)
				RadialIntType2 =  (32.*Sqrt(0.4)*q*Z1**1.5*Z2**3.5*							&
								  ((5.*Z1*(11.*Z1 - 3.*Z2)*(Z1 - Z2)*(Z1 + Z2)**4)/81. + 	 &
								  27.*q**6*(5.*Z1 + 2.*Z2) - 								  &
								  (q**2*(Z1 - 2.*Z2)*(Z1 + Z2)**2*							   &
								  (35.*Z1**2 - 18.*Z1*Z2 + 3.*Z2**2))/9. - 						&
								  3.*q**4*(25.*Z1**3 + 42.*Z1**2*Z2 + 9.*Z1*Z2**2 - 4.*Z2**3)	 &
								  ))/(19683.*(q**2 + (Z1 + Z2)**2/9.)**6)  
			case(2)
				RadialIntType2 =  (32.*Sqrt(0.4)*q**2*Z1**1.5*Z2**3.5*							  &
								  (3645.*q**6 - 81.*q**4*(65.*Z1**2 + 48.*Z1*Z2 - 9.*Z2**2) + 	   &
								  (Z1 - Z2)*(Z1 + Z2)**3*											&
								  (85.*Z1**2 - 26.*Z1*Z2 + Z2**2) + 								 &
								  27.*q**2*(Z1 + Z2)*												  &
								  (5.*Z1**3 + 27.*Z1**2*Z2 - 9.*Z1*Z2**2 + Z2**3)))/				   &
								  (9.*q**2 + (Z1 + Z2)**2)**6  
		end select
	endif

! �������� �� 3p
	if( k1.eq.301 .and. k2.eq.301 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =  (-64.*(Z1*Z2)**2.5*(6561.*q**8 - 						 &
								  1458.*q**6*(3.*Z1**2 + 12.*Z1*Z2 + 7.*Z2**2) - 		  &
								  486.*q**4*Z1*(2.*Z1**3 - 11.*Z1*Z2**2 - 9.*Z2**3) - 	   &
								  18.*q**2*(Z1 + Z2)**3*									&
								  (Z1**3 - 15.*Z1**2*Z2 + 27.*Z1*Z2**2 - 7.*Z2**3) + 		 &
								  (Z1 + Z2)**5*(3.*Z1**3 - 15.*Z1**2*Z2 + 11.*Z1*Z2**2 - 	  &
								  Z2**3)))/(9.*(9.*q**2 + (Z1 + Z2)**2)**6)  
			case(1)
				RadialIntType2 =  (-128.*q*(Z1*Z2)**2.5*									&
								  (-729.*q**6*(3.*Z1 + 4.*Z2) - 							 &
								  243.*q**4*(Z1**3 - 4.*Z1**2*Z2 - 8.*Z1*Z2**2 - 			  &
								  2.*Z2**3) + (Z1 + Z2)**4*									   &
								  (3.*Z1**3 - 16.*Z1**2*Z2 + 14.*Z1*Z2**2 - 2.*Z2**3) + 		&
								  9.*q**2*(Z1 + Z2)**2*											 &
								  (3.*Z1**3 + 6.*Z1**2*Z2 - 31.*Z1*Z2**2 + 8.*Z2**3)))/			  &
								  (3.*(9.*q**2 + (Z1 + Z2)**2)**6)   
			case(2)
				RadialIntType2 =  (-256.*q**2*(Z1*Z2)**2.5*									 &
								  (-729.*q**6 + 162.*q**4*Z2*(3.*Z1 + 2.*Z2) + 				  &
								  2.*(Z1 - Z2)*(Z1 + Z2)**3*(Z1**2 - 5.*Z1*Z2 + Z2**2) + 	   &
								  27.*q**2*(Z1**4 - 6.*Z1**2*Z2**2 - 4.*Z1*Z2**3 + Z2**4)))		&
								  /(9.*q**2 + (Z1 + Z2)**2)**6  
			
		
			
		end select
	endif

	if( k1.eq.301 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType2 =  (-64.*Z1**2.5*Z2**3.5*									 &
								  ((5.*Z1*(Z1 - Z2)*(Z1 + Z2)**5)/81. + 					  &
								  9.*q**6*(5.*Z1 + 4.*Z2) - 								   &
								  (5.*q**2*(Z1 + Z2)**3*(7.*Z1**2 - 9.*Z1*Z2 + 4.*Z2**2))/		&
								  27. - (q**4*(Z1 + Z2)*										 &
								  (35.*Z1**2 + 73.*Z1*Z2 + 8.*Z2**2))/3.))/						  &
								  (19683.*Sqrt(5.)*(q**2 + (Z1 + Z2)**2./9.)**6)
			case(1)
				RadialIntType2 =  (-128.*q*Z1**2.5*Z2**3.5*										  &
								  (15.*q**6 + (5.*(Z1 + Z2)**4*									   &
								  (10.*Z1**2 - 10.*Z1*Z2 + Z2**2))/243. - 							&
								  (q**2*(Z1 + Z2)**2*(5.*Z1**2 - 26.*Z1*Z2 + 11.*Z2**2))/			 &
								  9. - q**4*(20.*Z1**2 + 34.*Z1*Z2 + 11.*Z2**2)))/					  &
								  (19683.*Sqrt(5.)*(q**2 + (Z1 + Z2)**2/9.)**6)  
			case(2)
				RadialIntType2 = (128.*q**2*Z1**2.5*Z2**3.5*									   &
								 (243.*q**4*(5.*Z1 + 4.*Z2) - 										&
								 18.*q**2*(Z1 + Z2)*(5.*Z1**2 + 13.*Z1*Z2 - 4.*Z2**2) - 			 &
								 (Z1 + Z2)**3*(25.*Z1**2 - 27.*Z1*Z2 + 4.*Z2**2)))/					  &
								 (Sqrt(5.)*(9.*q**2 + (Z1 + Z2)**2)**6)  
			
			case(3)
				RadialIntType2 = (2304.*q**3*Z1**2.5*Z2**3.5*									&	
								 (135.*q**4 - 6.*q**2*(5.*Z1**2 + 6.*Z1*Z2 - Z2**2) - 			 &
								 (Z1 + Z2)**2*(5.*Z1**2 - 6.*Z1*Z2 + Z2**2)))/					  &
								 (Sqrt(5.)*(9.*q**2 + (Z1 + Z2)**2)**6)  
		end select
	endif

	

! �������� �� 3d
	
	if( k1.eq.302 .and. k2.eq.302 ) then
		select case(lambda)
			case(0)
				RadialIntType2 = (64.*(-6.*Z1**3.5*Z2**4.5*(Z1 + Z2)*							    &
								 (81.*q**4 - 30.*q**2*(Z1 + Z2)**2 + (Z1 + Z2)**4) + 				&
								 (Z1*Z2)**3.5*(9.*q**2 + (Z1 + Z2)**2)*								&
								 (81.*q**4 - 90.*q**2*(Z1 + Z2)**2 + 5.*(Z1 + Z2)**4)))/			&
								 (9.*(9.*q**2 + (Z1 + Z2)**2)**6) 
			case(1)
				RadialIntType2 = (128.*q*(Z1*Z2)**3.5*												&
								 (5.*(5.*Z1 - 2.*Z2)*(Z1 + Z2)**4 - 								&
								 243.*q**4*(5.*Z1 + 6.*Z2) + 										&
								 18.*q**2*(Z1 + Z2)**2*(5.*Z1 + 26.*Z2)))/							&
								 (15.*(9.*q**2 + (Z1 + Z2)**2)**6) 
			case(2)
				RadialIntType2 = (128.*q**2*(Z1*Z2)**3.5*											&
								 (-405.*q**4 + 7.*(5.*Z1 - 3.*Z2)*(Z1 + Z2)**3 + 					&
								 54.*q**2*(Z1 + Z2)*(5.*Z1 + 9.*Z2)))/								&
								 (5.*(9.*q**2 + (Z1 + Z2)**2)**6)		
			case(3)
				RadialIntType2 = (3072.*q**3*(Z1*Z2)**3.5*											&
								 ((5.*Z1 - 4.*Z2)*(Z1 + Z2)**2 + 9.*q**2*(5.*Z1 + 6.*Z2)))/			&
								 (5.*(9.*q**2 + (Z1 + Z2)**2)**6)

			case(4)
				RadialIntType2 = (9216.*q**4*(Z1*Z2)**3.5*(9.*q**2 + Z1**2 - Z2**2))/				&
								 (9.*q**2 + (Z1 + Z2)**2)**6 
		end select
	endif




!=============================================================================================================================
END FUNCTION RadialIntType2


SUBROUTINE pw_me(qvec, Z1, n1, l1, m1, Z2, n2, l2, m2, Result)
!==============================================================================================
! 12.01.2007
! ��������� �������� ������� ���� � ��������� ���������� �������
!
! < n1, l1, m1| e^{i\vec{q}\vec{r} |n2, l2, m2 >
!
! ���������� ������� � ������ � ����� ��������� ����� ���� ����� ��� ������ �������
! Z1 - ����� ��� ������ ��������
! Z2 - ����� ��� ����� ��������
!
! 15.01.2007
! ���������� ���������� ����������
!
! 18.04.2007
! ��������, ����� ������! 
! ��� d-��������� �� ��������
!==============================================================================================
REAL, INTENT(IN) :: qvec(3)
REAL, INTENT(IN) :: Z1, Z2
INTEGER, INTENT(IN) :: n1, l1, m1
INTEGER, INTENT(IN) :: n2, l2, m2
COMPLEX, INTENT(OUT) :: RESULT 
INTEGER :: lambda
REAL, PARAMETER :: pi = 3.1415926535897932385
COMPLEX, PARAMETER :: IUNIT = (0.D0, 1.D0)
COMPLEX :: SPHER
COMPLEX :: SphFunc
REAL :: Rad, SphInt 
real :: q, theta_q, phi_q

CALL VectorAngles( qvec(1), qvec(2), qvec(3), q, theta_q, phi_q )

RESULT = (0.D0, 0.D0) 
DO lambda = ABS(l2 - l1), l2 + l1
   Rad = PW_RadialInt(Z1, n1, l1, Z2, n2, l2, lambda, q)
   SphInt = ThreeSpherHarmonicIntegral(l1, m1, lambda, m1-m2, l2, m2)
   SphFunc = dconjg(SPHER(lambda, m1 - m2, theta_q, phi_q))
   RESULT = RESULT + 4.D0*pi*(IUNIT**lambda) * Rad * SphInt * SphFunc
ENDDO

RETURN
!=============================================================================================
END SUBROUTINE pw_me

REAL FUNCTION PW_RadialInt(Z1_in, n1_in, l1_in, Z2_in, n2_in, l2_in, lambda_in, k_in)
!=============================================================================================================================
! 15.01.2007
! ���������� ��������� ��� ��������� ��������� ������� ���� � ���������
! ���������������� ������� � ������������ ������� ���� ������ � �����
!
!  18.04.2007
! ��������, ����� ������! 
! ��� d-��������� �� ��������
!=============================================================================================================================
REAL ,INTENT(IN) :: Z1_in, Z2_in, k_in
REAL  :: Z1, Z2, k
INTEGER, INTENT(IN) :: n1_in, l1_in, n2_in, l2_in, lambda_in
INTEGER :: n1, l1, n2, l2, lambda
REAL :: Result = 0.0
INTEGER :: l_buffer
REAL  :: Z_buffer
INTEGER :: n_buffer
INTEGER :: i_case

Z1 = Z1_in
Z2 = Z2_in
k = k_in
n1 = n1_in
l1 = l1_in
n2 = n2_in
l2 = l2_in
lambda = lambda_in


IF ((n1.gt.n2).OR.(n1.eq.n2.AND.l1.gt.l2)) THEN
	l_buffer = l2
	l2 = l1
	l1 = l_buffer
	Z_buffer = Z2
	Z2 = Z1
	Z1 = Z_buffer
	n_buffer = n2
	n2 = n1
	n1 = n_buffer
ENDIF

i_case = 100*n2 + l2

IF( n1.eq.1.AND.l1.eq.0 ) THEN
	SELECT CASE( i_case )
		CASE(100)
			Result = 8.* (Z1*Z2)**(3./2) * (Z1+Z2)/ ( k**2 + (Z1+Z2)**2 )**2
		CASE(200)
			Result = (32.*Sqrt(2.)*(Z1*Z2)**1.5*(4.*k**2*(Z1 + Z2) + & 
					 (Z1 - Z2)*(2.*Z1 + Z2)**2))/(4.*k**2 + (2.*Z1 + Z2)**2)**3
		CASE(201)
			Result = (128.*Sqrt(2./3)*k*Z1**1.5*Z2**2.5*(2.*Z1 + Z2))/(4.*k**2 + (2.*Z1 + Z2)**2)**3
		CASE(300)
			Result = (72.*Sqrt(3.)*(Z1*Z2)**1.5*(3.*k**2 + (Z1 - Z2)*(3.*Z1 + Z2))*  &
				     (27.*k**2*(Z1 + Z2) + (3.*Z1 - Z2)*(3.*Z1 + Z2)**2))/(9.*k**2 + (3.*Z1 + Z2)**2)**4
		CASE(301)
			Result = (288.*Sqrt(6.)*k*Z1**1.5*Z2**2.5*(9.*k**2*(2.*Z1 + Z2) + & 
					 (2.*Z1 - Z2)*(3.*Z1 + Z2)**2))/(9.*k**2 + (3.*Z1 + Z2)**2)**4
		CASE(302)
			Result = (1728.*Sqrt(1.2)*k**2*Z1**1.5*Z2**3.5*(3.*Z1 + Z2))/(9.*k**2 + (3.*Z1 + Z2)**2)**4 
	END SELECT
ENDIF

IF( n1.eq.2.AND.l1.eq.0 ) THEN
	SELECT CASE( i_case )
		CASE(200)
			Result = (-16.*(Z1*Z2)**1.5*(Z1 + Z2)*(-2.*k + Z1 + Z2)* &
					(2.*k + Z1 + Z2)*(4.*k**2 + Z1**2 - 4.*Z1*Z2 + Z2**2))/(4.*k**2 + (Z1 + Z2)**2)**4
		CASE(201)
			Result = (64.*k*Z1**1.5*Z2**2.5*(-((4.*Z1 - Z2)*(Z1 + Z2)**2) + & 
					 4.*k**2*(2.*Z1 + Z2)))/(Sqrt(3.)*(4.*k**2 + (Z1 + Z2)**2)**4)
		CASE(300)
			Result = (-288.*Sqrt(6.)*(Z1*Z2)**1.5*(36.*k**2 + 9.*Z1**2 - 36.*Z1*Z2 + 4.*Z2**2)*(-1296.*k**4*(Z1 + Z2) + & 
					 120.*k**2*Z2*(3.*Z1 + 2.*Z2)**2 + (Z1 - Z2)*(3.*Z1 + 2.*Z2)**4))/(36.*k**2 + (3.*Z1 + 2.*Z2)**2)**5
		CASE(301)
			Result = (-9216.*Sqrt(3.)*k*Z1**1.5*Z2**2.5*(-1296.*k**4*(2.*Z1 + Z2) + 72.*k**2*Z1*(3.*Z1 + 2.*Z2)* &
					 (3.*Z1 + 11.*Z2) + (3.*Z1 + 2.*Z2)**3*(12.*Z1**2 - 19.*Z1*Z2 + 2.*Z2**2)))/(36.*k**2 + (3.*Z1 + 2.*Z2)**2)**5
		CASE(302)
			Result = (221184.*Sqrt(0.6)*k**2*Z1**1.5*Z2**3.5*(36.*k**2*(3.*Z1 + Z2) - (9.*Z1 - Z2)*(3.*Z1 + 2.*Z2)**2)) / & 
					 (36.*k**2 + (3.*Z1 + 2.*Z2)**2)**5
	END SELECT
ENDIF

IF( n1.eq.2.AND.l1.eq.1 ) THEN
	SELECT CASE( i_case )
		CASE(201)
			SELECT CASE( lambda )
				CASE( 0 )
					Result = (32.*(Z1*Z2)**2.5*(Z1 + Z2)*(-4.*k**2 + (Z1 + Z2)**2))/(4.*k**2 + (Z1 + Z2)**2)**4
				CASE( 1 )
					Result = (64.*k*(Z1*Z2)**2.5*(-4.*k**2 + 5.*(Z1 + Z2)**2))/(3.*(4.*k**2 + (Z1 + Z2)**2)**4)
				CASE( 2 )
					Result = (256.*k**2*(Z1*Z2)**2.5*(Z1 + Z2))/(4.*k**2 + (Z1 + Z2)**2)**4
			END SELECT
		CASE(300)
			Result = (3456.*Sqrt(2.)*k*Z1**2.5*Z2**1.5*(3888.*k**4*(Z1 + 2.*Z2) + & 
					  216.*k**2*(3.*Z1 + 2.*Z2)*(3.*Z1**2 - 4.*Z1*Z2 - 12.*Z2**2) + &
			         (3.*Z1 + 2.*Z2)**3*(9.*Z1**2 - 48.*Z1*Z2 + 44.*Z2**2)))/(36.*k**2 + (3.*Z1 + 2.*Z2)**2)**5
		CASE(301)
			SELECT CASE( lambda )
				CASE( 0 )
					Result = (13824.*(Z1*Z2)**2.5*(-1296.*k**4*(Z1 + Z2) + 120.*k**2*Z2*(3.*Z1 + 2.*Z2)**2 + & 
							 (Z1 - Z2)*(3.*Z1 + 2.*Z2)**4))/(36.*k**2 + (3.*Z1 + 2.*Z2)**2)**5
				CASE( 1 )
					Result = (9216.*k*(Z1*Z2)**2.5*(-1296.*k**4 + 5.*(3.*Z1 - 4.*Z2)*(3.*Z1 + 2.*Z2)**3 + & 
							 72.*k**2*(3.*Z1 + 2.*Z2)*(6.*Z1 + 13.*Z2)))/(36.*k**2 + (3.*Z1 + 2.*Z2)**2)**5
				CASE( 2 )
					Result = (331776.*k**2*(Z1*Z2)**2.5*(108.*k**2*(Z1 + Z2) + & 
							 (3.*Z1 - 5.*Z2)*(3.*Z1 + 2.*Z2)**2))/(36.*k**2 + (3.*Z1 + 2.*Z2)**2)**5
			END SELECT
		CASE(302)
			SELECT CASE( lambda )
				CASE( 1 )
					Result = (55296.*k*Z1**2.5*Z2**3.5*(3.*Z1 + 2.*Z2)*(-108.*k**2 + 5.*(3.*Z1 + 2.*Z2)**2)) / &
							 (Sqrt(5.)*(36.*k**2 + (3.*Z1 + 2.*Z2)**2)**5)
				CASE( 2 )
					Result = (-331776.*k**2*Z1**2.5*Z2**3.5*(36.*k**2 - 7.*(3.*Z1 + 2.*Z2)**2)) / &
							 (Sqrt(5.)*(36.*k**2 + (3.*Z1 + 2.*Z2)**2)**5)
				CASE( 3 )
					Result = (15925248.*k**3*Z1**2.5*Z2**3.5*(3.*Z1 + 2.*Z2))/(Sqrt(5.)*(36.*k**2 + (3.*Z1 + 2.*Z2)**2)**5)
			END SELECT
	END SELECT
ENDIF

IF( n1.eq.3.AND.l1.eq.0 )  THEN
	SELECT CASE( i_case )
		CASE(300)
			Result = (8.*(Z1*Z2)**1.5*(Z1 + Z2)*(3.*(9.*k**2 + Z1**2)**2 - 36.*Z1*(9.*k**2 + Z1**2)*Z2 + & 
					  2.*(27.*k**2 + 41.*Z1**2)*Z2**2 - 36.*Z1*Z2**3 + 3.*Z2**4) * &
				     (-27.*k**2 + (Z1 + Z2)**2)*(-3.*k**2 + (Z1 + Z2)**2))/(9.*k**2 + (Z1 + Z2)**2)**6
		CASE(301)
			Result =  (32.*Sqrt(2.)*k*Z1**1.5*Z2**2.5*(2187.*k**6*(2.*Z1 + Z2) + & 
					  (Z1 + Z2)**4*(22.*Z1**3 - 79.*Z1**2*Z2 + 36.*Z1*Z2**2 - 3.*Z2**3) - &
				      243.*k**4*(10.*Z1**3 + 27.*Z1**2*Z2 + 12.*Z1*Z2**2 - Z2**3) - & 
					  9.*k**2*(Z1 + Z2)**2*(14.*Z1**3 - 145.*Z1**2*Z2 + 12.*Z1*Z2**2 + 3.*Z2**3))) /	&
					  (9.*k**2 + (Z1 + Z2)**2)**6
		CASE(302)
			Result =  (64.*Sqrt(0.4)*k**2*Z1**1.5*Z2**3.5*(243.*k**4*(3.*Z1 + Z2) - & 
					   54.*k**2*(Z1 + Z2)*(13.*Z1**2 + 4.*Z1*Z2 - Z2**2) + (Z1 + Z2)**3*(73.*Z1**2 - 36.*Z1*Z2 + 3.*Z2**2))) / &
					  (177147.*(k**2 + (Z1 + Z2)**2/9.)**6)
	END SELECT
ENDIF

IF( n1.eq.3.AND.l1.eq.1 )  THEN
	SELECT CASE( i_case )
		CASE(301)
			SELECT CASE( lambda )
				CASE( 0 )
					Result =  (-128.*(Z1*Z2)**2.5*(Z1 + Z2)*(9.*k**2 + Z1**2 - 3.*Z1*Z2 + Z2**2)*(-27.*k**2 + & 
							  (Z1 + Z2)**2)*(-3.*k**2 + (Z1 + Z2)**2))/(9.*k**2 + (Z1 + Z2)**2)**6
				CASE( 1 )
					Result =  (-128.*k*(Z1*Z2)**2.5*(1458.*k**6 - 54.*k**2*(Z1 + Z2)**2* & 
							  (Z1**2 - 19.*Z1*Z2 + Z2**2) + 5.*(Z1 + Z2)**4*(4.*Z1**2 - 13.*Z1*Z2 + 4.*Z2**2) - & 
						      243.*k**4*(8.*Z1**2 + 19.*Z1*Z2 + 8.*Z2**2)))/(3.*(9.*k**2 + (Z1 + Z2)**2)**6)
				CASE( 2 ) 
					Result =  (-768.*k**2*(Z1*Z2)**2.5*(Z1 + Z2)*(-243.*k**4 + 18.*k**2*(Z1**2 + 8.*Z1*Z2 + Z2**2) + & 
							  (Z1 + Z2)**2*(5.*Z1**2 - 18.*Z1*Z2 + 5.*Z2**2)))/(9.*k**2 + (Z1 + Z2)**2)**6
			END SELECT
		CASE(302)
			SELECT CASE( lambda )
				CASE( 1 )
					Result = (-128.*k*Z1**2.5*Z2**3.5*(5.*(5.*Z1 - 2.*Z2)*(Z1 + Z2)**4 + 243.*k**4*(3.*Z1 + 2.*Z2) - & 
								18.*k**2*(Z1 + Z2)**2*(23.*Z1 + 2.*Z2)))/(531441.*Sqrt(5.)*(k**2 + (Z1 + Z2)**2/9.)**6)
				CASE( 2 )
					Result = (-256.*k**2*Z1**2.5*Z2**3.5*(81.*k**4 + 7.*(3.*Z1 - Z2)*(Z1 + Z2)**3 - & 
							 54.*k**2*(Z1 + Z2)*(3.*Z1 + Z2)))/(177147.*Sqrt(5.)*(k**2 + (Z1 + Z2)**2/9.)**6)
				CASE( 3 )
					Result = (9216.*k**3*Z1**2.5*Z2**3.5*(-((7.*Z1 - 2.*Z2)*(Z1 + Z2)**2) + 9.*k**2*(3.*Z1 + 2.*Z2))) / &
							 (Sqrt(5.)*(9.*k**2 + (Z1 + Z2)**2)**6)
			END SELECT
	END SELECT
ENDIF
 					    
IF( n1.eq.3.AND.l1.eq.2 )  THEN
	SELECT CASE( lambda )
		CASE( 0 )
			Result = (128.*(Z1*Z2)**3.5*(Z1 + Z2)*(81.*k**4 - 30.*k**2*(Z1 + Z2)**2 + (Z1 + Z2)**4))/(9.*k**2 + (Z1 + Z2)**2)**6
		CASE( 1	)
			Result = (128.*k*(Z1*Z2)**3.5*(243.*k**4 - 378.*k**2*(Z1 + Z2)**2 + 35.*(Z1 + Z2)**4))/(5.*(9.*k**2 + (Z1 + Z2)**2)**6)
		CASE( 2 )
			Result = (3072.*k**2*(Z1*Z2)**3.5*(Z1 + Z2)*(-27.*k**2 + 7.*(Z1 + Z2)**2))/(5.*(9.*k**2 + (Z1 + Z2)**2)**6)
		CASE( 3 )
			Result = (82944.*k**3*(Z1*Z2)**3.5*(-k**2 + (Z1 + Z2)**2))/(5.*(9.*k**2 + (Z1 + Z2)**2)**6)
		CASE( 4 )
			Result = (55296.*k**4*(Z1*Z2)**3.5*(Z1 + Z2))/(9.*k**2 + (Z1 + Z2)**2)**6
	END SELECT
ENDIF

PW_RadialInt = Result


!=============================================================================================================================
END FUNCTION PW_RadialInt


REAL FUNCTION U0fourier(G)
!==============================================================================================================
! ��������� �������
!==============================================================================================================
USE GLOBAL
REAL, INTENT(IN) :: G
REAL :: akoeff1

akoeff1 = 4.*pi/(2.*pi)**3 

U0fourier = 0.0
DO J = 1, 3
	U0fourier = U0fourier + a_j(J)/( G**2 + (b_j(J)/a_TF)**2 )
ENDDO
U0fourier = akoeff1 * Zt * U0fourier

RETURN
!==============================================================================================================
END FUNCTION U0fourier


