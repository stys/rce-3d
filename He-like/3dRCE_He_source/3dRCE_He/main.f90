! ����������� ����������� ����������� ������������� �����
! � ��������� ������� ��� ������� �������������.
! 
! ������ �.�.
! 11.02.2009 
!------------------------------------------------------------------------------------------------------

MODULE Global
!======================================================================================================
! ����������� ���������� ����������
!====================================================================================================== 

USE States
implicit none

real*8, parameter :: pi = 3.1415926535897932385
complex, parameter :: iunit = (0., 1.)

! ������� �������
real, parameter :: c_au = 137.035999	! �������� ����� � �.�.
real, parameter :: E0 = 27.2114			! ��
real, parameter :: a0 = 0.529177E-8		! ��
real, parameter :: t0 = 2.41888E-17		! ���

! ��������� ����
real :: Zion 
real :: Vion, gamma
real, dimension(3) :: v

! ����� ��������� � ������������� |n1 l1, n2 l2, LSJMj>
integer :: nmax, nbasis 
integer, allocatable, dimension(:, :) :: LS_terms
real, allocatable, dimension(:, :) :: Zscr	! �������������� ������

type(state), dimension(:), allocatable :: st_basis

! ������� �������
real, dimension(:), allocatable :: E

! ������� ���������
real, allocatable, dimension(:,:) :: omega

! ������� ����-������������ ��������������
complex, allocatable, dimension(:,:) :: W_ls

! ��������� ���������
real :: a_lattice 
real :: r_sqr_mean   
real, parameter :: Zt = 14.

real :: akoeff

real :: path_mkm, path_au	! ������� �������� � �������� � � �.�.
real :: time_lab, time_ion	! ����� ������ � ��� ������� � � ������� ���� 

! ����� �� ������� 
integer :: Npoints_t
real :: dt
real, allocatable, dimension(:) :: t_mesh

! ���� ��
real :: fixed_angle

! ����� �� ���� ������������
integer :: iscan
integer :: Npoints_scan
real :: scan_min_deg, scan_min_rad
real :: scan_max_deg, scan_max_rad
real :: dscan
real, allocatable, dimension(:) :: scan_mesh
integer :: Npoints_time_resolved
integer, allocatable, dimension(:) :: Numbers_time_resolved

! ��������� ���������� �������
real, dimension(3) :: a_j, b_j
real :: a_TF

! ��������� ���������� �����-�������
REAL, DIMENSION(4) ::  DT_d_a, DT_d_b, DT_p_a, DT_p_b
REAL :: DT_d_c
REAL, DIMENSION(10) ::  DT_p_s_i, DT_p_f_i, DT_d_s_i, DT_d_f_i

! ������� ������������ ��������
integer :: n_res
integer, allocatable, dimension(:, :) :: res_index
integer, allocatable, dimension(:, :) :: amp_include

real, allocatable, dimension(:, :) :: G_klm, G1_klm
complex, allocatable, dimension( : ) :: Fklm
complex, allocatable, dimension( : ) :: strf
complex, allocatable, dimension( :, :, : ) :: Amp1, Amp2
complex, allocatable, dimension( :, :, : ) :: V1, V2, V3
complex, allocatable, dimension( :, : ) :: VRes_sum

! ����� ����������
integer, allocatable, dimension(:, :, :) :: mask

! �� ������ ��������� ������������ ������� �����
integer :: Nscale

! ���������
real :: dens_e_c, dens_i_c	! � ���������
real :: dens_e_f, dens_i_f	! � ������

! �������
real, allocatable, dimension(:) :: cs_ion_heavy  ! ��������� � ������� �������������
real, allocatable, dimension(:) :: cs_ion_elec   ! ��������� � ����������� �������������
real, allocatable, dimension(:,:) :: cs_exc_heavy ! ����������� - ������������� � ������� �������������
real, allocatable, dimension(:,:) :: cs_exc_elec	! ����������� - ������������� � ����������� �������������

real, allocatable, dimension(:,:) :: alambda_decay ! ������������ ������
real, allocatable, dimension(:) :: alambda_auto

real, allocatable, dimension(:) :: alambda_ion_heavy, alambda_ion_elec
real, allocatable, dimension(:,:) :: alambda_pq ! �������� ��������� ����� �����������
real, allocatable, dimension(:) :: alambda_tot ! ������ �������� ������ �� ���������

! ��������� ��������
real :: dfree, dt_free
integer :: Npoints_free
real :: time_lab_free, time_ion_free

! �������������� ������
integer :: ifoil
real :: dfree2
real :: path_f_mkm, path_f_au
real :: time_f_lab, time_f_ion	
real, allocatable, dimension(:) :: cs_ion_heavy_f, cs_ion_elec_f  
real, allocatable, dimension(:,:) :: cs_exc_heavy_f, cs_exc_elec_f

! ����� �� ������� ��� �������������� ������ 
integer :: Npoints_t_f
real :: dt_f
real, allocatable, dimension(:) :: t_mesh_f

! ������� �������������
! � ������ ������ �������������� ������ �������� �������������
! ������� �� 1s2p:1P1 ���������, � ��� �� ���������� � �������
! �� ������������������ ��������� 2p^2:1D2.

! ������� ��������� 1s2p:1P1 � ������
integer, dimension(-1:1) :: j_1P1  ! ������ ��������� ��������� 1s2p:1P1 � ������
integer, dimension(-2:2) :: j_1D2

! ����� ������ �������� �������������
integer :: key_1P1, key_1D2

! ���������� ���������� � �����������
integer :: Ndetectors
real, allocatable, dimension(:, :) :: detector_angles

! ������ �������� ������������� � �������� ������
integer :: Npoints_angular
integer, allocatable, dimension(:) :: Numbers_angular
character*64 :: ang_mesh_file
integer :: flag_mesh
! ��� �����
integer :: Npoints_ang
real, allocatable, dimension(:, :) :: ang_mesh

!---------------------------------------------------------------------------------------------------------

CONTAINS 

	SUBROUTINE Setup
	!-----------------------------------------------------------------------------------------------------
	integer :: j, k, j_t, j_s, p, q, jj
	real ::  t, scan
	
	character*1 :: c
	integer :: ios, cb, flag, flag2, sign
	integer, dimension(5) :: n

		
	! ������ ����� �����
	open(1, file='input.txt')
	do j = 1, 9	
		read(1,*)
	enddo	
	
	! ��������� ����	
	read(1, *)	Zion 
	read(1, *)
	read(1, *) 

	read(1, *)	Vion 	   
	read(1, *)
	read(1, *)
	gamma = 1./dsqrt(1.- Vion**2/c_au**2) 

	print*, " 3D Resonant Coherent Excitation "
	print*, " Helium-like ion"
	print*, " Z_ion = ", Zion	
	print*
	print*, " Velocity (a.u.): ", Vion
	print*, " gamma:", gamma
	
	! ������������ ������� ��������� ����� ��� ��������������� ���������
	read(1, *) nmax
	read(1, *)
	read(1, *)

	! ����� ��������� � ������
	read(1, *) nbasis
	read(1, *)
	read(1, *)
	read(1, *)

	! ���������� LS-������ ��� ������� ��������� ���������
	allocate( LS_terms(nbasis, 8) )
	allocate( Zscr(nbasis, 2) )
	do j = 1, nbasis
		read(1, *) ( LS_terms(j, k), k = 1, 8 ), (Zscr(j,k), k = 1, 2)
	enddo  
	read(1, *)
	read(1, *)
	
	! ��������� �������� ��� ������ 
	allocate( st_basis(nbasis) )
	allocate( E(nbasis) )
	allocate( omega(nbasis, nbasis) )
	
	! ������� ������� � �������
	do j = 1, nbasis
		read(1, *) E(j)
		E(j) = E(j)/E0	! ������� �� ������������� � ������� �������
	enddo

	do p = 1, nbasis
		do q = 1, nbasis
			omega(p,q) = E(p) - E(q)
		enddo
	enddo
	read(1, *)
	read(1, *)

	! ������� ����-������������ ��������������
	allocate( W_ls(nbasis, nbasis) )
	do j = 1, nbasis
		read(1, *) ( W_ls(j, k), k = 1, nbasis )
	enddo
	read(1, *)
	read(1, *)

	! ������������ ��������� � ������� �� ��������
	read(1, *) n_res
	read(1, *)
	read(1, *)
	read(1, *)
			
	allocate( res_index(n_res, 3) )
	allocate( amp_include(n_res, 100) )  
		! � ����� ����������� � ���������������� ������ 
	do j = 1, n_res
		jj = 0
		ios = 0
		flag = 0
		flag2 = 1
		sign = 1
		do while(ios.eq.0) 
			read(1, '(A1)', IOSTAT = ios, ADVANCE = 'NO') c
			
			if(flag2.le.3) then
				cb = ICHAR(c) - ICHAR('0')
				if(cb.eq.-3) then
					sign = -1
				endif				
				if(cb.ge.0.and.cb.le.9) then
					flag = flag+1
					do k = 1, flag
						n(5 - flag + k - 1) = n(5 - flag + k)
					enddo
					n(5) = cb
				else
					if( flag.ge.1 ) then
						res_index(j, flag2) = sign*(n(1)*10000 + n(2)*1000 + n(3)*100 + n(4)*10 + n(5))
						flag = 0
						flag2 = flag2 + 1
					endif
					if(cb.ne.-3.and.cb.ne.-16) then
						sign = 1
					endif					
					n = 0
				endif
			
			else
				cb = ICHAR(c) - ICHAR('0')
				if(cb.ge.0.and.cb.le.9) then
					flag = flag+1
					do k = 1, flag
						n(5 - flag + k - 1) = n(5 - flag + k)
					enddo
					n(5) = cb
				else
					if( flag.ge.1 ) then
						jj = jj + 1
						amp_include(j, jj) = n(1)*10000 + n(2)*1000 + n(3)*100 + n(4)*10 + n(5) 
						flag = 0
					endif
					n = 0
				endif
			endif
		
		enddo
			
	enddo	
	
	read(1, *)
	read(1, *)

	! ��������� ��������	
	allocate( G_klm(n_res, 3) )
	allocate( G1_klm(n_res, 3) )
	allocate( Fklm(n_res) )
	allocate( strf(n_res) )
	allocate( Amp1(n_res, nbasis, nbasis) )
	allocate( Amp2(n_res, nbasis, nbasis) )
	allocate( V1(n_res, nbasis, nbasis) )
	allocate( V2(n_res, nbasis, nbasis) )
	allocate( V3(n_res, nbasis, nbasis) )
	allocate( VRes_sum(nbasis, nbasis) )  

	G_klm	       = 0.0
	G1_klm		   = 0.0
	Fklm		   = 0.0
	strf		   = 0.0
	Amp1     = 0.0
	Amp2    = 0.0
	V1		   = 0.0
	V2		   = 0.0
	V3		   = 0.0
	VRes_sum	   = 0.0

	! ����� ���������, �� �������� ������������ ������� �����
	read(1, *) Nscale
	read(1, *)
	read(1, *)

	! �� ������ ���� �����������
	read(1, *) iscan
	read(1, *)
	read(1, *)

	read(1, *) scan_min_deg, scan_max_deg
	scan_min_rad = scan_min_deg * pi/180.
	scan_max_rad = scan_max_deg * pi/180.
	read(1, *)
	read(1, *)

	! ����� �� ���� ������������
	read(1, *)	Npoints_scan 
	allocate(scan_mesh(Npoints_scan))
	dscan = (scan_max_rad - scan_min_rad)/(Npoints_scan - 1.)
	scan = scan_min_rad
	do j_s = 1, Npoints_scan
		scan_mesh(j_s) = scan
		scan = scan + dscan
	enddo
	read(1, *)
	read(1, *)

	! �����, � ������� ����� ������ ����������� �� ������� ������������� �������
	read(1, *) Npoints_time_resolved
	if( Npoints_time_resolved.eq.0 ) then
		do j = 1, 5
			read(1, *)
		enddo
	else
		allocate( Numbers_time_resolved(Npoints_time_resolved) )
		read(1, *)
		read(1, *)
		read(1, *) 	(Numbers_time_resolved(k), k = 1, Npoints_time_resolved)
		read(1, *)
		read(1, *)
	endif

	! ������������� ����
	read(1, *) fixed_angle
	fixed_angle = fixed_angle * pi/180.
	read(1, *)
	read(1, *)

	! ���������� ���������� � �����������
	read(1, *) Ndetectors
	read(1, *)
	read(1, *)

	allocate( detector_angles(Ndetectors, 2) )
	do j = 1, Ndetectors
		read(1, *) 	detector_angles(j, 1), detector_angles(j, 2)
		detector_angles(j,1) = 	detector_angles(j,1) * pi/180.  ! ������� �� �������� � �������
		detector_angles(j,2) = 	detector_angles(j,2) * pi/180.
	enddo
	read(1, *)
	read(1, *)

	! �����, � ������� ����� ������ ������� �������������
	read(1, *) Npoints_angular
	if( Npoints_angular.eq.0 ) then
		do j = 1, 5
			read(1, *)
		enddo
	else
		allocate( Numbers_angular(Npoints_angular) )
		read(1, *)
		read(1, *)
		read(1, *) 	(Numbers_angular(k), k = 1, Npoints_angular)
		read(1, *)
		read(1, *)
	endif

	! �� ����� ����� �������� ������� �������������
	read(1, *) ang_mesh_file
	read(1, *)
	read(1, *)
	if( ang_mesh_file.eq.'default') then
		flag_mesh = 1
	else
		flag_mesh = 0
	
		open(2, file = ang_mesh_file)
		read(2, *) Npoints_ang
		allocate( ang_mesh(Npoints_ang, 2) )
		do j = 1, Npoints_ang
			read(2, *) ang_mesh(j, 1), ang_mesh(j, 2)
		enddo
		close(2)

	endif

	! ������� ������
	read(1, *) path_mkm 	 
	path_au	= path_mkm * 1.E-4/(a0)
	time_lab = path_au/Vion
	time_ion = time_lab/gamma
	read(1, *)
	read(1, *)
	
	! ����� �� �������
	read(1, *)	Npoints_t 
	allocate(t_mesh(Npoints_t))
	dt = time_ion/(Npoints_t - 1.)
	t = 0.0
	do j_t = 1, Npoints_t
		t_mesh(j_t) = t
		t = t + dt
	enddo
	read(1, *)
	read(1, *)

	print*
	print*, " Crystal:"
	print*, " thickness (mkm):", path_mkm
	print*, " thickness (a.u.):", path_au
	print*, " time of flight, lab system, a.u.:", time_lab
	print*, " time of flight, ion system, a.u.:", time_ion  

	! ��������� ���������
	read(1, *) a_lattice, r_sqr_mean 	 
	akoeff = 2.*pi/a_lattice
	read(1, *)
	read(1, *)

	! ��������� ���������� �������
	read(1, *)	(a_j(k), k = 1, 3)
	read(1, *)  (b_j(k), k = 1, 3)
	a_TF = 0.885/Zt**(1./3.)
	read(1, *)
	read(1, *)
	
	! �������
	allocate( cs_ion_heavy(nbasis), alambda_ion_heavy(nbasis)  ) 
	allocate( cs_ion_elec(nbasis), alambda_ion_elec(nbasis) ) 
	allocate( cs_exc_heavy(nbasis, nbasis) ) 
	allocate( cs_exc_elec(nbasis, nbasis) ) 
	allocate( alambda_decay(nbasis, nbasis) ) 
	allocate( alambda_auto(nbasis) ) 
	allocate( alambda_pq(nbasis, nbasis) )
	allocate( alambda_tot(nbasis) )  

	! ��������� ������
	read(1, *) dens_i_c, dens_e_c
	read(1, *)
	read(1, *)

	! ���������
	do j = 1, nbasis
		read(1, *)	cs_ion_heavy(j), cs_ion_elec(j) 	
	enddo
	read(1, *)
	read(1, *)

	! ���������������� ��������
	do j = 1, nbasis
		read(1, *) (cs_exc_heavy(j,k), k = 1, nbasis)
	enddo 
	read(1, *)
	read(1, *)

	do j = 1, nbasis
		read(1, *) (cs_exc_elec(j,k), k = 1, nbasis)
	enddo 
	read(1, *)
	read(1, *)

	! ���������� ������������
	do j = 1, nbasis
		read(1, *) (alambda_decay(j,k), k = 1, nbasis)
	enddo
	read(1, *)
	read(1, *)

	! �������������
	do j = 1, nbasis
		read(1, *) alambda_auto(j)
	enddo
	read(1, *)
	read(1, *)

	! ��������� ��������
	read(1, *) dfree, Npoints_free
	read(1, *)
	read(1, *)
	
	dfree = dfree * 1.E-4/(a0)
	time_lab_free = dfree/Vion
	time_ion_free = time_lab_free/gamma
	
	dt_free = time_ion_free/(Npoints_free - 1.)


	! �������������� ������
	read(1, *) ifoil
	read(1, *)
	read(1, *)

	if(ifoil.eq.1) then	
		
		allocate( cs_ion_heavy_f(nbasis)  ) 
		allocate( cs_ion_elec_f(nbasis) ) 
		allocate( cs_exc_heavy_f(nbasis, nbasis) ) 
		allocate( cs_exc_elec_f(nbasis, nbasis) ) 
				
		! ���������� �� ������
		read(1, *) dfree2
		read(1, *)
		read(1, *)		
		
		! ������� ������
		read(1, *) path_f_mkm 	 
		path_f_au	= path_f_mkm * 1.E-4/(a0)
		time_f_lab = path_f_au/Vion
		time_f_ion = time_f_lab/gamma
		read(1, *)
		read(1, *)

		! ����� �� �������
		read(1, *)	Npoints_t_f 
		allocate(t_mesh_f(Npoints_t_f))
		dt_f = time_f_ion/(Npoints_t_f - 1.)
		t = 0.0
		do j_t = 1, Npoints_t_f
			t_mesh_f(j_t) = t
			t = t + dt_f
		enddo
		read(1, *)
		read(1, *)
		
		! ��������� ������
		read(1, *) dens_i_f, dens_e_f
		read(1, *)
		read(1, *)

		! ���������
		do j = 1, nbasis
			read(1, *)	cs_ion_heavy_f(j), cs_ion_elec_f(j) 	
		enddo
		read(1, *)
		read(1, *)


		! ���������������� ��������
		do j = 1, nbasis
			read(1, *) (cs_exc_heavy_f(j,k), k = 1, nbasis)
		enddo 
		read(1, *)
		read(1, *)

		do j = 1, nbasis
			read(1, *) (cs_exc_elec_f(j,k), k = 1, nbasis)
		enddo 
		!read(1, *)
		!read(1, *)

	endif
	
	!-----------------------------------------------------------------------------------------------------
	END SUBROUTINE Setup

!=========================================================================================================
END MODULE Global

PROGRAM Main
!**********************************************************************************************************************
USE Global  
Implicit None

integer :: j_s, j, jj, p, q
integer :: f1   ! ����
integer :: line1, line2, col1, col2

CALL Tabular

! �������� ������ 
CALL Setup

! �������� ��������������� ������������, ����������� ��� ������������ � n<=2
CALL TableConf(nmax)  

! �������� ��������� 
do j = 1, nbasis
	CALL StateConstructor(Zscr(j, 1), LS_terms(j, 1), LS_terms(j, 2), 	 &
						  Zscr(j, 2), LS_terms(j, 3), LS_terms(j, 4),  	 &
						  LS_terms(j, 5),  LS_terms(j, 6), LS_terms(j, 7), LS_terms(j, 8), st_basis(j))  
enddo


! ����������� ������� ��������� ��������� 1s2p:1P1 � ������
j_1P1 = 0
key_1P1 = 1
do j = 1, nbasis
	if( LS_terms(j, 1).eq.1 .and. LS_terms(j, 2).eq.0 .and.  	&
		LS_terms(j, 3).eq.2 .and. LS_terms(j, 4).eq.1 .and.		&
		LS_terms(j, 5).eq.1 .and. LS_terms(j, 6).eq.0 .and. LS_terms(j, 7).eq.1 ) then
			select case ( LS_terms(j, 8) ) 
				case(-1)
					j_1P1(-1) = j
				case( 0)
					j_1P1( 0) = j
				case( 1)		  
					j_1P1( 1) = j
			end select
	endif
enddo

f1 = 0
do j = - 1, 1
	if( j_1P1(j).eq.0 ) then
		f1 = f1 + 1
	endif
enddo
if(f1.gt.0) then
	print*, 'Warning: some 1s2p:1P1 components not found. Angular distributions will not be calculated.'
	key_1P1 = 0
	pause
endif
	 
! ����������� ������� ��������� ��������� 2p^2:1D2 � ������
j_1D2 = 0
key_1D2 = 1
do j = 1, nbasis
	if( LS_terms(j, 1).eq.2 .and. LS_terms(j, 2).eq.1 .and.  	&
		LS_terms(j, 3).eq.2 .and. LS_terms(j, 4).eq.1 .and.		&
		LS_terms(j, 5).eq.2 .and. LS_terms(j, 6).eq.0 .and. LS_terms(j, 7).eq.2 ) then
			select case ( LS_terms(j, 8) ) 
				case(-2)
					j_1D2(-2) = j
				case(-1)
					j_1D2(-1) = j
				case( 0)
					j_1D2( 0) = j
				case( 1)		  
					j_1D2( 1) = j
				case( 2)		  
					j_1D2( 2) = j
			end select
	endif
enddo

f1 = 0
do j = - 2, 2
	if( j_1D2(j).eq.0 ) then
		f1 = f1 + 1
	endif
enddo
if(f1.gt.0.and.f1.lt.5) then
	print*, 'Warning: some 2p^2:1D2 components not found. Angular distributions will not be calculated.'
	key_1D2 = 0
	pause
endif
if(f1.eq.5) then
	key_1D2 = 0
endif

! ������ ������ � ���������� � ����
open( 101, file = 'output/info/basis.txt' )
do j = 1, nbasis
	CALL WriteOutState(101, st_basis(j))
enddo
close(101)

! ������ �������������
CALL CalcFklm

! ����� ����������
allocate( mask(n_res, nbasis, nbasis) )

do j = 1, n_res
	mask(j, :, :) = 0
	do jj = 1, 100, 4
		if( amp_include(j, jj).ne.0 ) then  
			line1 = amp_include(j, jj)
			line2 = amp_include(j, jj + 1)
			 col1 =	amp_include(j, jj + 2)
			 col2 =	amp_include(j, jj + 3)
			do p = 1, nbasis
				do q = 1, nbasis
					if( p.ge.line1 .and. p.le.line2	.and. q.ge.col1	.and. q.le.col2 ) then
						mask(j, p, q) = 1
						mask(j, q, p) = 1
					endif
				enddo
			enddo		
		else 
			exit
		endif
	enddo
enddo

! ����� ������
open(121, file = 'surv_frac.dat')
open(122, file = 'tot_yields.dat')
open(123, file = 'dif_yields.dat')
open(125, file = 'frac_after_crystal.dat')
open(126, file = 'frac_after_free.dat')
open(127, file = 'frac_after_foil.dat')

! ������� ������� ���������
print*
print*, 'Calculating...'
do j_s = 1, Npoints_scan 	
	print*, 'point: ', j_s
	CALL CalcTimeDependence(j_s)
enddo

deallocate(mask)
!**********************************************************************************************************************
END

SUBROUTINE CalcTimeDependence(j_s)
!----------------------------------------------------------------------------------------------------------------------
USE Global
Implicit None

integer, intent(in) :: j_s
real :: Surv_wof, Surv_free, Surv_wif
real :: Tot_ph1P1_in, Tot_e1D2_in, Tot_ph1D2_in
real :: Tot_ph1P1_out, Tot_e1D2_out, Tot_ph1D2_out
complex :: W_in_ph1P1(Ndetectors), W_in_e1D2(Ndetectors), W_in_ph1D2(Ndetectors) 
complex :: W_out_ph1P1(Ndetectors), W_out_e1D2(Ndetectors), W_out_ph1D2(Ndetectors) 
complex :: W, W1, W2, W3

complex, allocatable, dimension(:, :) :: dm, dm_after_crystal, dm_previous
complex, dimension( - 1 : 1, - 1 : 1 ) :: dm_1P1
complex, dimension( - 2 : 2, - 2 : 2 ) :: dm_1D2

real :: t, escale
integer :: p, j_t, q, n, j, jj, j_s_tres, k, j_s_ang, j_a 

complex :: rho_00, rho_00_a, rho_00_b
complex, dimension( - 1 : 1) :: rho_1q, rho_1q_a, rho_1q_b
complex, dimension( - 2 : 2) :: rho_2q, rho_2q_a, rho_2q_b
complex, dimension( - 3 : 3) :: rho_3q, rho_3q_b 
complex, dimension( - 4 : 4) :: rho_4q, rho_4q_b

real :: th, ph, d_ang

complex :: SPHER

integer :: n1, n2, n3
character*64 :: filename_frac_tres		! ��� ����� ��� ������ ������������ �� ������� �������
character*64 :: filename_angXY, filename_angYZ, filename_angZX	! ����� ������ ��� ������ ������� �������������
character*64 :: filename_mesh

EXTERNAL EqSystem_crystal ! ������� ��������� ��� �������� � ��������������� ������
EXTERNAL EqSystem_free	  ! ������� ��� ���������� �������� ( � ������ ���������� ��������, ������������ ��������� � ���-�������)
EXTERNAL EqSystem_foil	  ! ������� ��� �������������� ������

INTERFACE
	SUBROUTINE Tensors(J, Dm, rho_00, rho_1q, rho_2q, rho_3q, rho_4q)
		integer, intent(IN) :: J
		complex, intent(IN), dimension( - J : J, - J : J ) :: Dm
		complex, intent(OUT) :: rho_00
		complex, intent(OUT), dimension( - 1 : 1) :: rho_1q
		complex, intent(OUT), dimension( - 2 : 2) :: rho_2q
		complex, intent(OUT), dimension( - 3 : 3), optional :: rho_3q
		complex, intent(OUT), dimension( - 4 : 4), optional :: rho_4q
	END SUBROUTINE Tensors
END INTERFACE

allocate( dm(nbasis, nbasis), dm_after_crystal(nbasis, nbasis), dm_previous(nbasis, nbasis) )

v(1) = Vion 
v(2) = 0.0
v(3) = 0.0

! ������� �������� ������� � ������� � ���� � �� �����
if (iscan .eq. 1) then
	do n = 1, n_res
		G_klm(n, 1) = akoeff*( dsqrt(2.)*res_index(n, 1)*cos(scan_mesh(j_s))*cos(fixed_angle) &
								  + res_index(n, 2)*sin(scan_mesh(j_s)) + dsqrt(2.)*res_index(n, 3)*cos(scan_mesh(j_s))*sin(fixed_angle)  ) 
		G_klm(n, 2) = akoeff*(- dsqrt(2.)*res_index(n, 1)*sin(scan_mesh(j_s))*cos(fixed_angle) & 
								  + res_index(n, 2)*cos(scan_mesh(j_s)) - dsqrt(2.)*res_index(n, 3)*sin(scan_mesh(j_s))*sin(fixed_angle)  )
		G_klm(n, 3) = akoeff*(- dsqrt(2.)*res_index(n, 1)*sin(fixed_angle) + dsqrt(2.)*res_index(n, 3)*cos(fixed_angle) )  
	enddo
endif

if (iscan .eq. 2) then
	do n = 1, n_res
		G_klm(n, 1) = akoeff*( dsqrt(2.)*res_index(n, 1)*cos(fixed_angle)*cos(scan_mesh(j_s)) &
								  + res_index(n, 2)*sin(fixed_angle) + dsqrt(2.)*res_index(n, 3)*cos(fixed_angle)*sin(scan_mesh(j_s))  ) 
		G_klm(n, 2) = akoeff*(- dsqrt(2.)*res_index(n, 1)*sin(fixed_angle)*cos(scan_mesh(j_s)) & 
								  + res_index(n, 2)*cos(fixed_angle) - dsqrt(2.)*res_index(n, 3)*sin(fixed_angle)*sin(scan_mesh(j_s))  )
		G_klm(n, 3) = akoeff*(- dsqrt(2.)*res_index(n, 1)*sin(scan_mesh(j_s)) + dsqrt(2.)*res_index(n, 3)*cos(scan_mesh(j_s)) )  
	enddo
endif

! ������ �������� ������������ ��������
CALL CalcResAmp

! ���������� �������� ��������� !
do j = 1, n_res

	do p = 1, nbasis
		do q = 1, nbasis
			if( mask(j, p, q).eq.0 ) then
				Amp1(j, p, q) = 0.0
				Amp2(j, p, q) = 0.0
			endif
		enddo

	enddo
	
enddo


! ������� ���������� ����������
V1 = 0.0
do n = 1, n_res
	do p = 1, nbasis
		do q = 1, nbasis
			V1(n, p, q) = - gamma * Fklm(n) * Amp1(n, p, q)
		enddo
	enddo
enddo

! ������� ���������� A_x d/dx
do n = 1, n_res
	do p = 1, nbasis
		do q = 1, nbasis
			V2(n, p, q) = iunit*gamma*(Vion/c_au**2) * Fklm(n) * Amp2(n, p, q)
		enddo
	enddo
enddo

! ������� ���������� div(A) 
do n = 1, n_res
	do p = 1, nbasis
		do q = 1, nbasis
			V3(n, p, q) = - gamma*(Vion/(2.*c_au**2)) * Fklm(n) * G1_klm(n, 1) * Amp1(n, p, q)
		enddo
	enddo
enddo

! ������� ���������
t = 0.0
dm = (0., 0.)
dm(1, 1) = (1.0, 0.0)

dm_1P1 = (0., 0.)
dm_1D2 = (0., 0.)

! �������� ��� �� �������� ������� �� ������� ��� ������ �����
j_s_tres = 0
do j = 1, Npoints_time_resolved
	if( j_s.eq.Numbers_time_resolved(j) ) then
		j_s_tres = j_s
	endif
enddo

if( j_s_tres.ne.0 ) then
	n1 = (j_s_tres - MOD(j_s_tres, 100))/100
	n2 = ( (j_s_tres - n1*100) - MOD((j_s_tres - n1*100), 10))/10
	n3 = MOD(j_s_tres - n1*100 - n2*10, 10 )

	filename_frac_tres = 'output/frac/'//'frac'//CHAR(ICHAR('0')+n1)//CHAR(ICHAR('0')+n2)//CHAR(ICHAR('0')+n3)//'.dat'
	
	open(112, file = filename_frac_tres)
endif
112 format(1p, 200(e16.8e3, 1x))

! ������ � ���� ���������� �������������
if( j_s_tres.ne.0 ) then
	write(112, 112)	0.0, (dreal(dm(k, k)), k = 1, nbasis)
endif

! ��������
do j_t = 1, Npoints_t

	dm_previous = dm

	! ��� �������
	CALL RungeKutta4( t, dt, dm, dm, EqSystem_crystal )
	t = t + dt
	
	! �������������� ������� ��������� �������� ���������
	if( key_1P1.eq. 1 ) then
		do p = - 1, 1
			do q = -1, 1
				dm_1P1(p, q) = dm_1P1(p, q) + ( dm(j_1P1(p), j_1P1(q)) + dm_previous(j_1P1(p), j_1P1(q)) )  * dt /2.
			enddo
		enddo
	endif
	
	if( key_1D2.eq. 1 ) then
		do p = - 2, 2
			do q = -2, 2
				dm_1D2(p, q) = dm_1D2(p, q) + ( dm(j_1D2(p), j_1D2(q)) + dm_previous(j_1D2(p), j_1D2(q)) )  * dt /2.
			enddo
		enddo
	endif

	
		
	! ������ ������� � ����
	if( j_s_tres.ne.0 ) then
		write(112, 112)	t, (dreal(dm(k, k)), k = 1, nbasis)
	endif

enddo

! ���������� ����� ����������� ��������������� ������
	
	dm_after_crystal = dm

	! ������� ��������� � ������������� ������� 
	Surv_wof = 0.0
	do p = 1, nbasis
		Surv_wof = Surv_wof + dreal( dm(p, p) )
	enddo

	! ������ ������ ������� � ���������� ������ ������
	Tot_ph1P1_in = 0.0
	if( key_1P1.eq.1 ) then
		do p = -1, 1
			Tot_ph1P1_in = Tot_ph1P1_in + dreal( dm_1P1(p, p) )
		enddo
		Tot_ph1P1_in = Tot_ph1P1_in * alambda_decay(j_1P1(1), 1) 	
	endif

	Tot_ph1D2_in = 0.0
	if( key_1D2.eq.1 ) then
		do p = - 2, 2
			Tot_ph1D2_in = Tot_ph1D2_in + dreal( dm_1D2(p, p) )
		enddo
		Tot_e1D2_in	 = Tot_ph1D2_in * alambda_auto( j_1D2(2) )    ! ������ ����� ���-���������� ������ ������
		Tot_ph1D2_in = Tot_ph1D2_in * alambda_decay(j_1D2(2), 1)  ! ������ ����� ������� �� 2p^2:1D2 ��������� ������ ������	
	endif

	! ���������������� ����� ��� ������� ���������
	W_in_ph1P1 = 0.0
	W_in_e1D2 = 0.0
	W_in_ph1D2 = 0.0 
	
	! ������ �� 1s2p:1P1 ���������
	if( key_1P1.eq.1 ) then
		do j = 1, Ndetectors
			CALL Tensors(1, dm_1P1, rho_00, rho_1q, rho_2q)
			
			do q = - 2, 2
				 W_in_ph1P1(j) = W_in_ph1P1(j) + 1./dsqrt(2.) * dsqrt( 4.*pi/5. ) * rho_2q(q) * SPHER(2, q, detector_angles(j,1), detector_angles(j,2))
			enddo
			W_in_ph1P1(j) = alambda_decay(j_1P1(1), 1) * dsqrt(3.)*(rho_00 + W_in_ph1P1(j))/(4.*pi)	
		enddo
	endif
	
	! 2p^2 : 1D2			
	if( key_1D2.eq.1 ) then
		do j = 1, Ndetectors
			CALL Tensors(2, dm_1D2, rho_00, rho_1q, rho_2q, rho_3q, rho_4q)
			
			! ������ �� 2p^2:1D2 ���������
			do q = - 2, 2
				 W_in_ph1D2(j) = W_in_ph1D2(j) + ( dsqrt(7.)/(2.*dsqrt(10.)) ) * dsqrt( 4.*pi/5. ) * rho_2q(q) * SPHER(2, q, detector_angles(j,1), detector_angles(j,2))
			enddo
			
			W_in_ph1D2(j) = alambda_decay(j_1D2(2), j_1P1(1)) * dsqrt(5.)*(rho_00 + W_in_ph1D2(j))/(4.*pi)	
		
			! ��������� �� 2p^2:1D2 ���������
			do q = - 2, 2
				W_in_e1D2(j) = W_in_e1D2(j) - dsqrt(2./7.) * dsqrt(4.*pi) * rho_2q(q)*SPHER(2, q, detector_angles(j,1), detector_angles(j,2) )
			enddo
			do q = - 4, 4
				W_in_e1D2(j) = W_in_e1D2(j) + dsqrt(2./7.) * dsqrt(4.*pi) * rho_4q(q)*SPHER(4, q, detector_angles(j,1), detector_angles(j,2) )
			enddo
			W_in_e1D2(j) = alambda_auto(j_1D2(2)) * dsqrt(5.)*(rho_00 + W_in_e1D2(j) )/(4.*pi)	   
		enddo
	endif

	! ������ � ����
	write(125, 121) scan_mesh(j_s) * 180./pi, escale, (dreal(dm_after_crystal(p,p)), p = 1, nbasis)

	! ������� ������������� �� ������� ������ ������
	
	! �������� ��� �� �������� ������� ������������� ��� ������ �����
	j_s_ang = 0
	do j = 1, Npoints_angular
		if( j_s.eq.Numbers_angular(j) ) then
			j_s_ang = j_s
		endif
	enddo

	if( j_s_ang.ne.0 ) then
		n1 = (j_s_ang - MOD(j_s_ang, 100))/100
		n2 = ( (j_s_ang - n1*100) - MOD((j_s_ang - n1*100), 10))/10
		n3 = MOD(j_s_ang - n1*100 - n2*10, 10 )

		filename_angXY = 'output/ang/'//'angXY'//CHAR(ICHAR('0')+n1)//CHAR(ICHAR('0')+n2)//CHAR(ICHAR('0')+n3)//'.dat'
		filename_angYZ = 'output/ang/'//'angYZ'//CHAR(ICHAR('0')+n1)//CHAR(ICHAR('0')+n2)//CHAR(ICHAR('0')+n3)//'.dat'
		filename_angZX = 'output/ang/'//'angZX'//CHAR(ICHAR('0')+n1)//CHAR(ICHAR('0')+n2)//CHAR(ICHAR('0')+n3)//'.dat'
		filename_mesh = 'output/ang/'//'angM'//CHAR(ICHAR('0')+n1)//CHAR(ICHAR('0')+n2)//CHAR(ICHAR('0')+n3)//'.dat'

		211 format(1p, 5(e16.8e3, 1x))

		if(flag_mesh.eq.1) then

			open(211, file = filename_angXY)
			open(212, file = filename_angYZ)
			open(213, file = filename_angZX)
			
			if( key_1P1.eq.1 ) then
				CALL Tensors(1, dm_1P1, rho_00_a, rho_1q_a, rho_2q_a)
			endif
			
			if( key_1D2.eq.1 ) then
				CALL Tensors(2, dm_1D2, rho_00_b, rho_1q_b, rho_2q_b, rho_3q_b, rho_4q_b)
			endif		

			! � ��������� XY
			th = pi/2.
			ph = 0.0
			d_ang = pi/180.
			do j_a = 1, 361
					
				! ������ �� 1s2p:1P1
				if( key_1P1.eq.1 ) then
					do q = - 2, 2
						 W1 = W1 + 1./dsqrt(2.) * dsqrt( 4.*pi/5. ) * rho_2q_a(q) * SPHER(2, q, th, ph)
					enddo
					W1 = alambda_decay(j_1P1(1), 1) * dsqrt(3.)*(rho_00_a + W1)/(4.*pi)	
				else
					W1 = 0.0
				endif
				
				! ������ �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						 W2 = W2 + ( dsqrt(7.)/(2.*dsqrt(10.)) ) * dsqrt( 4.*pi/5. ) * rho_2q_b(q) * SPHER(2, q, th, ph)
					enddo
					W2 = alambda_decay(j_1D2(2), 1) * dsqrt(5.)*(rho_00_b + W2)/(4.*pi)	
				else
					W2 = 0.0
				endif

				! ��������� �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						W3 = W3 - dsqrt(2./7.) * dsqrt(4.*pi) * rho_2q_b(q)*SPHER(2, q, th, ph)
					enddo
					do q = - 4, 4
						W3 = W3 + dsqrt(2./7.) * dsqrt(4.*pi) * rho_4q_b(q)*SPHER(4, q, th, ph)
					enddo
					W3 = alambda_auto(j_1D2(2)) * dsqrt(5.)*(rho_00_b + W3)/(4.*pi)	   
				else
					W3 = 0.0
				endif
				
				write(211, 211) ph*180./pi, dreal(W1), dreal(W2), dreal(W3)

				ph = ph + d_ang
			enddo				
			
			! � ��������� YZ
			ph = pi/2.
			th = 0.0
			d_ang = pi/180.
			do j_a = 1, 181
				
				! ������ �� 1s2p:1P1
				if( key_1P1.eq.1 ) then
					do q = - 2, 2
						 W1 = W1 + 1./dsqrt(2.) * dsqrt( 4.*pi/5. ) * rho_2q_a(q) * SPHER(2, q, th, ph)
					enddo
					W1 = alambda_decay(j_1P1(1), 1) * dsqrt(3.)*(rho_00_a + W1)/(4.*pi)	
				else
					W1 = 0.0
				endif
				
				! ������ �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						 W2 = W2 + ( dsqrt(7.)/(2.*dsqrt(10.)) ) * dsqrt( 4.*pi/5. ) * rho_2q_b(q) * SPHER(2, q, th, ph)
					enddo
					W2 = alambda_decay(j_1D2(2), 1) * dsqrt(5.)*(rho_00_b + W2)/(4.*pi)	
				else
					W2 = 0.0
				endif

				! ��������� �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						W3 = W3 - dsqrt(2./7.) * dsqrt(4.*pi) * rho_2q_b(q)*SPHER(2, q, th, ph)
					enddo
					do q = - 4, 4
						W3 = W3 + dsqrt(2./7.) * dsqrt(4.*pi) * rho_4q_b(q)*SPHER(4, q, th, ph)
					enddo
					W3 = alambda_auto(j_1D2(2)) * dsqrt(5.)*(rho_00_b + W3)/(4.*pi)	   
				else
					W3 = 0.0
				endif

				write(212, 211) th*180./pi, dreal(W1), dreal(W2), dreal(W3)

				th = th + d_ang
			enddo				
			do j_a = 181, 361
				! ������ �� 1s2p:1P1
				if( key_1P1.eq.1 ) then
					do q = - 2, 2
						 W1 = W1 + 1./dsqrt(2.) * dsqrt( 4.*pi/5. ) * rho_2q_a(q) * SPHER(2, q, 2.*pi - th, ph + pi)
					enddo
					W1 = alambda_decay(j_1P1(1), 1) * dsqrt(3.)*(rho_00_a + W1)/(4.*pi)	
				else
					W1 = 0.0
				endif
				
				! ������ �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						 W2 = W2 + ( dsqrt(7.)/(2.*dsqrt(10.)) ) * dsqrt( 4.*pi/5. ) * rho_2q_b(q) * SPHER(2, q, 2.*pi - th, ph + pi)
					enddo
					W2 = alambda_decay(j_1D2(2), 1) * dsqrt(5.)*(rho_00_b + W2)/(4.*pi)	
				else
					W2 = 0.0
				endif

				! ��������� �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						W3 = W3 - dsqrt(2./7.) * dsqrt(4.*pi) * rho_2q_b(q)*SPHER(2, q, 2.*pi - th, ph + pi)
					enddo
					do q = - 4, 4
						W3 = W3 + dsqrt(2./7.) * dsqrt(4.*pi) * rho_4q_b(q)*SPHER(4, q, 2.*pi - th, ph + pi)
					enddo
					W3 = alambda_auto(j_1D2(2)) * dsqrt(5.)*(rho_00_b + W3)/(4.*pi)	   
				else
					W3 = 0.0
				endif

				write(212, 211) th*180./pi, dreal(W1), dreal(W2), dreal(W3)

				th = th + d_ang
			enddo				
			
			! � ��������� ZX	
			ph = 0.0
			th = 0.0
			d_ang = pi/180.
			do j_a = 1, 181
				
				! ������ �� 1s2p:1P1
				if( key_1P1.eq.1 ) then
					do q = - 2, 2
						 W1 = W1 + 1./dsqrt(2.) * dsqrt( 4.*pi/5. ) * rho_2q_a(q) * SPHER(2, q, th, ph)
					enddo
					W1 = alambda_decay(j_1P1(1), 1) * dsqrt(3.)*(rho_00_a + W1)/(4.*pi)	
				else
					W1 = 0.0
				endif
				
				! ������ �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						 W2 = W2 + ( dsqrt(7.)/(2.*dsqrt(10.)) ) * dsqrt( 4.*pi/5. ) * rho_2q_b(q) * SPHER(2, q, th, ph)
					enddo
					W2 = alambda_decay(j_1D2(2), j_1P1(1)) * dsqrt(5.)*(rho_00_b + W2)/(4.*pi)	
				else
					W2 = 0.0
				endif

				! ��������� �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						W3 = W3 - dsqrt(2./7.) * dsqrt(4.*pi) * rho_2q_b(q)*SPHER(2, q, th, ph)
					enddo
					do q = - 4, 4
						W3 = W3 + dsqrt(2./7.) * dsqrt(4.*pi) * rho_4q_b(q)*SPHER(4, q, th, ph)
					enddo
					W3 = alambda_auto(j_1D2(2)) * dsqrt(5.)*(rho_00_b + W3)/(4.*pi)	   
				else
					W3 = 0.0
				endif

				write(213, 211) th*180./pi, dreal(W1), dreal(W2), dreal(W3)

				th = th + d_ang
			enddo				
			do j_a = 181, 361
				! ������ �� 1s2p:1P1
				if( key_1P1.eq.1 ) then
					do q = - 2, 2
						 W1 = W1 + 1./dsqrt(2.) * dsqrt( 4.*pi/5. ) * rho_2q_a(q) * SPHER(2, q, 2.*pi - th, ph + pi)
					enddo
					W1 = alambda_decay(j_1P1(1), 1) * dsqrt(3.)*(rho_00_a + W1)/(4.*pi)	
				else
					W1 = 0.0
				endif
				
				! ������ �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						 W2 = W2 + ( dsqrt(7.)/(2.*dsqrt(10.)) ) * dsqrt( 4.*pi/5. ) * rho_2q_b(q) * SPHER(2, q, 2.*pi - th, ph + pi)
					enddo
					W2 = alambda_decay(j_1D2(2), j_1P1(1)) * dsqrt(5.)*(rho_00_b + W2)/(4.*pi)	
				else
					W2 = 0.0
				endif

				! ��������� �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						W3 = W3 - dsqrt(2./7.) * dsqrt(4.*pi) * rho_2q_b(q)*SPHER(2, q, 2.*pi - th, ph + pi)
					enddo
					do q = - 4, 4
						W3 = W3 + dsqrt(2./7.) * dsqrt(4.*pi) * rho_4q_b(q)*SPHER(4, q, 2.*pi - th, ph + pi)
					enddo
					W3 = alambda_auto(j_1D2(2)) * dsqrt(5.)*(rho_00_b + W3)/(4.*pi)	   
				else
					W3 = 0.0
				endif

				write(213, 211) th*180./pi, dreal(W1), dreal(W2), dreal(W3)

				th = th + d_ang
			enddo				
		
			close(211)
			close(212)
			close(213)
					
		else
		
			open(211, file = filename_mesh)
		
			if( key_1P1.eq.1 ) then
				CALL Tensors(1, dm_1P1, rho_00_a, rho_1q_a, rho_2q_a)
			endif
			
			if( key_1D2.eq.1 ) then
				CALL Tensors(2, dm_1D2, rho_00_b, rho_1q_b, rho_2q_b, rho_3q_b, rho_4q_b)
			endif		

			
			do j_a = 1, Npoints_ang
				
				! ������ �� 1s2p:1P1
				if( key_1P1.eq.1 ) then
					do q = - 2, 2
						 W1 = W1 + 1./dsqrt(2.) * dsqrt( 4.*pi/5. ) * rho_2q_a(q) * SPHER(2, q, ang_mesh(j_a, 1), ang_mesh(j_a, 2))
					enddo
					W1 = alambda_decay(j_1P1(1), 1) * dsqrt(3.)*(rho_00_a + W1)/(4.*pi)	
				else
					W1 = 0.0
				endif
				
				! ������ �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						 W2 = W2 + ( dsqrt(7.)/(2.*dsqrt(10.)) ) * dsqrt( 4.*pi/5. ) * rho_2q_b(q) * SPHER(2, q, ang_mesh(j_a, 1), ang_mesh(j_a, 2))
					enddo
					W2 = alambda_decay(j_1D2(2), j_1P1(1)) * dsqrt(5.)*(rho_00_b + W2)/(4.*pi)	
				else
					W2 = 0.0
				endif

				! ��������� �� 2p^2:1D2 ���������
				if( key_1D2.eq.1 ) then
					do q = - 2, 2
						W3 = W3 - dsqrt(2./7.) * dsqrt(4.*pi) * rho_2q_b(q)*SPHER(2, q, ang_mesh(j_a, 1), ang_mesh(j_a, 2))
					enddo
					do q = - 4, 4
						W3 = W3 + dsqrt(2./7.) * dsqrt(4.*pi) * rho_4q_b(q)*SPHER(4, q, ang_mesh(j_a, 1), ang_mesh(j_a, 2))
					enddo
					W3 = alambda_auto(j_1D2(2)) * dsqrt(5.)*(rho_00_b + W3)/(4.*pi)	   
				else
					W3 = 0.0
				endif

				write(211, 211) ang_mesh(j_a, 1), ang_mesh(j_a, 2), dreal(W1), dreal(W2), dreal(W3)
				
			enddo				
			
			close(211)

		endif	
	endif

! ��������� ��������
dm_1P1 = 0.0
dm_1D2 = 0.0

do j_t = 1, Npoints_free

	dm_previous = dm

	! ��� �������
	CALL RungeKutta4( t, dt_free, dm, dm, EqSystem_free )
	t = t + dt_free
	
	! �������������� ������� ��������� �������� ���������
	if( key_1P1.eq. 1 ) then
		do p = - 1, 1
			do q = -1, 1
				dm_1P1(p, q) = dm_1P1(p, q) + ( dm(j_1P1(p), j_1P1(q)) + dm_previous(j_1P1(p), j_1P1(q)) ) * dt_free / 2.
			enddo
		enddo
	endif
	
	if( key_1D2.eq. 1 ) then
		do p = - 2, 2
			do q = -2, 2
				dm_1D2(p, q) = dm_1D2(p, q) + ( dm(j_1D2(p), j_1D2(q)) + dm_previous(j_1D2(p), j_1D2(q)) ) * dt_free / 2.
			enddo
		enddo
	endif
	
	! ������ ���������� ������� � ����
	!if( j_s_tres.ne.0 ) then
	!	write(112, 112)	t, (dreal(dm(k, k)), k = 1, nbasis)
	!endif

enddo

! ���������� ����� ����� ���������� ��������

	! ������� ��������� � ������������� ������� 
	Surv_free = 0.0
	do p = 1, nbasis
		Surv_free = Surv_free + dreal( dm(p, p) )
	enddo

	! ������ ������ ������� � ���������� ������� ������
	Tot_ph1P1_out = 0.0
	if( key_1P1.eq.1 ) then
		do p = -1, 1
			Tot_ph1P1_out = Tot_ph1P1_out + dreal( dm_1P1(p, p) )
		enddo
		Tot_ph1P1_out = Tot_ph1P1_out * alambda_decay(j_1P1(1), 1) 	
	endif

	Tot_ph1D2_out = 0.0
	if( key_1D2.eq.1 ) then
		do p = - 2, 2
			Tot_ph1D2_out = Tot_ph1D2_out + dreal( dm_1D2(p, p) )
		enddo
		Tot_e1D2_out = Tot_ph1D2_out * alambda_auto( j_1D2(2) )    ! ������ ����� ���-���������� ������ ������
		Tot_ph1D2_out = Tot_ph1D2_out * alambda_decay(j_1D2(2), 1)  ! ������ ����� ������� �� 2p^2:1D2 ��������� ������ ������	
	endif

	! ���������������� ����� ��� ������� ���������
	W_out_ph1P1 = 0.0
	W_out_e1D2 = 0.0
	W_out_ph1D2 = 0.0 
	
	! ������ �� 1s2p:1P1 ���������
	if( key_1P1.eq.1 ) then
		do j = 1, Ndetectors
			CALL Tensors(1, dm_1P1, rho_00, rho_1q, rho_2q)
		
			do q = - 2, 2
				 W_out_ph1P1(j) = W_out_ph1P1(j) + 1./dsqrt(2.) * dsqrt( 4.*pi/5. ) * rho_2q(q) * SPHER(2, q, detector_angles(j,1), detector_angles(j,2))
			enddo
			W_out_ph1P1(j) = alambda_decay(j_1P1(1), 1) * dsqrt(3.)*(rho_00 + W_out_ph1P1(j))/(4.*pi)	
		enddo
	endif
	
	! 2p^2 : 1D2			
	if( key_1D2.eq.1 ) then
		do j = 1, Ndetectors
			CALL Tensors(2, dm_1D2, rho_00, rho_1q, rho_2q, rho_3q, rho_4q)
			
			! ������ �� 2p^2:1D2 ���������
			do q = - 2, 2
				 W_out_ph1D2(j) = W_out_ph1D2(j) + ( dsqrt(7.)/(2.*dsqrt(10.)) ) * dsqrt( 4.*pi/5. ) * rho_2q(q) * SPHER(2, q, detector_angles(j,1), detector_angles(j,2))
			enddo
			
			W_out_ph1D2(j) = alambda_decay(j_1D2(2), j_1P1(1)) * dsqrt(5.)*(rho_00 + W_out_ph1D2(j))/(4.*pi)	
		
			! ��������� �� 2p^2:1D2 ���������
			do q = - 2, 2
				W_out_e1D2(j) = W_out_e1D2(j) - dsqrt(2./7.) * dsqrt(4.*pi) * rho_2q(q)*SPHER(2, q, detector_angles(j,1), detector_angles(j,2) )
			enddo
			do q = - 4, 4
				W_out_e1D2(j) = W_out_e1D2(j) + dsqrt(2./7.) * dsqrt(4.*pi) * rho_4q(q)*SPHER(4, q, detector_angles(j,1), detector_angles(j,2) )
			enddo
			W_out_e1D2(j) = alambda_auto(j_1D2(2)) * dsqrt(5.)*(rho_00 + W_out_e1D2(j) )/(4.*pi)	   
		enddo
	endif

	! ������ � ����		
	if( iscan .eq. 1 ) then
		escale = akoeff*gamma*Vion*(dsqrt(2.)*(res_index(Nscale, 1)*cos(fixed_angle) + res_index(Nscale, 3)*sin(fixed_angle)) &
				 * cos(scan_mesh(j_s)) + res_index(Nscale, 2) * sin(scan_mesh(j_s))) * E0
	endif
	if( iscan .eq. 2 ) then
		escale = akoeff*gamma*Vion*(dsqrt(2.)*(res_index(Nscale, 1)*cos(scan_mesh(j_s)) + res_index(Nscale, 3)*sin(scan_mesh(j_s))) &
				 * cos(fixed_angle) + res_index(Nscale, 2) * sin(fixed_angle)) * E0
	endif

	write(126, 121) scan_mesh(j_s) * 180./pi, escale, (dreal(dm(p,p)), p = 1, nbasis)
	
	write(122, 122) scan_mesh(j_s) * 180./pi, escale, Tot_ph1P1_in, Tot_ph1P1_out, & 
													  Tot_ph1D2_in, Tot_ph1D2_out, Tot_e1D2_in, Tot_e1D2_out    

	write(123, 123) scan_mesh(j_s) * 180./pi, escale, (dreal(W_in_ph1P1(j)), dreal(W_out_ph1P1(j)), j = 1, Ndetectors), &
													  (dreal(W_in_ph1D2(j)), dreal(W_out_ph1D2(j)), j = 1, Ndetectors), &
													  (dreal(W_in_e1D2(j)), dreal(W_out_e1D2(j)), j = 1, Ndetectors)
	
	
	121 format(1p, 100(e12.4e3, 1x))
	122 format(1p, 100(e12.4e3, 1x))
	123 format(1p, 100(e12.4e3, 1x))


! ������

if(ifoil.eq.1) then
	
	do j_t = 1, Npoints_t_f
		! ��� �������
		CALL RungeKutta4( t, dt_f, dm, dm, EqSystem_foil)
		t = t + dt_f

	enddo

	! ���������� ����� ����������� ������
	Surv_wif = 0.0
	do p = 1, nbasis
		Surv_wif = Surv_wif + dreal( dm(p, p) )
	enddo
else
	Surv_wif = 0.0
endif


! ������ � ����
write(121, 121) scan_mesh(j_s) * 180./pi, escale, Surv_wof, Surv_free, Surv_wif
write(127, 121) scan_mesh(j_s) * 180./pi, escale, (dreal(dm(p,p)), p = 1, nbasis)

deallocate( dm, dm_after_crystal, dm_previous )
!-----------------------------------------------------------
END SUBROUTINE CalcTimeDependence


SUBROUTINE RungeKutta4(t, dt1, dm, dm_out, EqSystem )
!============================================================================================
! ����� �����-����� 4��� �������. 
! �������� EqSystem	- ��� ������������ � ������� �������� ������� ���������
!============================================================================================
USE Global
Implicit None
real, intent(in) :: t, dt1 
complex, dimension( nbasis, nbasis ), intent(in) :: dm
complex, dimension( nbasis, nbasis ), intent(out) :: dm_out

real :: t1, t2
complex, dimension( nbasis, nbasis ) :: dmprime
complex, dimension( nbasis, nbasis ) :: ak1, ak2, ak3, ak4
complex, dimension( nbasis, nbasis ) :: dm1, dm2, dm3

INTERFACE
	SUBROUTINE EqSystem(t, dm, dmprime) 
	USE Global
	real, intent(in) :: t
	complex, dimension(nbasis, nbasis), intent(in) :: dm
	complex, dimension(nbasis, nbasis), intent(out) :: dmprime
	END SUBROUTINE EqSystem
END INTERFACE

CALL EqSystem(t, dm, dmprime)
ak1 = dmprime * dt1

t1 = t + dt1/2.
dm1 = dm + ak1/2.
CALL EqSystem(t1, dm1, dmprime)
ak2 = dmprime * dt1

dm2 = dm + ak2/2.
CALL EqSystem(t1, dm2, dmprime)
ak3 = dmprime * dt1

t2 = t + dt1
dm3 = dm + ak3
CALL EqSystem(t2, dm3, dmprime)
ak4 = dmprime * dt1

dm_out = dm + (1./6.) * (ak1 + 2.*ak2 + 2.*ak3 + ak4)

RETURN
!============================================================================================ 
END SUBROUTINE RungeKutta4

SUBROUTINE EqSystem_crystal(t, dm, dmprime) 
!============================================================================================
! ������� ��� �������� � ��������������� ������
!============================================================================================
USE Global
real, intent(in) :: t
complex, dimension(nbasis, nbasis), intent(in) :: dm
complex, dimension(nbasis, nbasis), intent(out) :: dmprime

real, dimension(n_res) :: scalp
integer :: p, q, r, n
real :: sigma, c

do n = 1, n_res
	scalp(n) = v(1)* G_klm(n,1) +  v(2) * G_klm(n,2) +  v(3)* G_klm(n,3)
enddo


! ������� ��������������
sigma = 25.

VRes_sum = (0., 0.)
do n = 1, n_res
	do p = 1, nbasis
		do q =	p, nbasis
			if( abs( gamma*scalp(n) + omega(p, q) ) .gt. sigma ) then 
				c = 0.0
			else
				c = 1.0
			endif		
			Vres_sum(p, q) = Vres_sum(p, q) + c * (V1(n, p, q) + V2(n, p, q) + V3(n, p, q)) & 
								                  * cdexp(iunit*gamma*scalp(n)*t) * cdexp(iunit * omega(p, q) * t)

			Vres_sum(q, p) = dconjg( Vres_sum(p, q) )
		
		enddo																													
	
	   
	enddo
enddo

! ������� ����-������
do p = 1, nbasis
	do q = 1, nbasis
		Vres_sum(p, q) = Vres_sum(p, q) + W_ls(p, q) * cdexp(iunit * omega(p, q) * t)
	enddo
enddo


! �������� ���������
alambda_ion_heavy = 0.0
alambda_ion_elec = 0.0 
do p = 1, nbasis
	alambda_ion_heavy(p) = cs_ion_heavy(p) * Vion * dens_i_c * gamma
	alambda_ion_elec(p)	 = cs_ion_elec(p)  * Vion * dens_e_c * gamma
enddo


alambda_pq = 0.0
do p = 1, nbasis
	do q = 1, nbasis
		alambda_pq(p,q) = alambda_decay(p,q) + cs_exc_heavy(p, q) * Vion * dens_i_c * gamma &
											 + cs_exc_elec(p, q)  * Vion * dens_e_c * gamma
	enddo
enddo

alambda_tot = 0.0 
do p = 1, nbasis
		alambda_tot(p) = alambda_ion_heavy(p) + alambda_ion_elec(p)	+ alambda_auto(p)
		do q = 1, nbasis
			alambda_tot(p) = alambda_tot(p) + alambda_pq(p, q)
		enddo
enddo


! ������������ ��������
do p = 1, nbasis
	dmprime(p, p) = (0., 0.)
	
	! ������������
	do r = 1, nbasis
		dmprime(p, p) = dmprime(p, p) + 2.0 * dimag( Vres_sum(p, r) * dm(r, p) )
	enddo
	

	! �������������� ����
	dmprime(p, p) = dmprime(p, p) - alambda_tot(p) * dm(p, p) 
	do r = 1, nbasis
		if(r.ne.p) then
			dmprime(p, p) = dmprime(p, p) + alambda_pq(r, p) * dm(r, r) 
		endif
	enddo
enddo


! �������������� ��������
do p = 1, nbasis
	do q = 1, nbasis
		if(p.ne.q) then
			dmprime(p, q) = (0., 0.)

			! ������������
			do r = 1, nbasis
				dmprime(p, q) = dmprime(p, q) - iunit * ( Vres_sum(p, r) * dm(r, q) -  dm(p, r) * Vres_sum(r, q) )
			enddo

			! �������������� ����
			dmprime(p, q) = dmprime(p, q) - (1./2.) * (alambda_tot(p) + alambda_tot(q)) * dm(p, q) 
		
		endif
	enddo
enddo

RETURN
!==================================================================================================================
END SUBROUTINE EqSystem_crystal


SUBROUTINE EqSystem_free(t, dm, dmprime) 
!==================================================================================================================
! ������� ��������� ��� ���������� ��������
!==================================================================================================================
USE Global
real, intent(in) :: t
complex, dimension(nbasis, nbasis), intent(in) :: dm
complex, dimension(nbasis, nbasis), intent(out) :: dmprime

integer :: p, q, r, n

Vres_sum = 0.0

! ������� ����-������
do p = 1, nbasis
	do q = 1, nbasis
		Vres_sum(p, q) = Vres_sum(p, q) + W_ls(p, q) * cdexp(iunit * omega(p, q) * t)
	enddo
enddo


! �������� ���������
alambda_ion_heavy = 0.0
alambda_ion_elec = 0.0 


alambda_pq = 0.0
do p = 1, nbasis
	do q = 1, nbasis
		alambda_pq(p,q) = alambda_decay(p,q) 
	enddo
enddo

alambda_tot = 0.0 
do p = 1, nbasis
		alambda_tot(p) = alambda_auto(p)
		do q = 1, nbasis
			alambda_tot(p) = alambda_tot(p) + alambda_pq(p, q)
		enddo
enddo


! ������������ ��������
do p = 1, nbasis
	dmprime(p, p) = (0., 0.)
	
	! ������������
	do r = 1, nbasis
		dmprime(p, p) = dmprime(p, p) + 2.0 * dimag( Vres_sum(p, r) * dm(r, p) )
	enddo
	

	! �������������� ����
	dmprime(p, p) = dmprime(p, p) - alambda_tot(p) * dm(p, p) 
	do r = 1, nbasis
		if(r.ne.p) then
			dmprime(p, p) = dmprime(p, p) + alambda_pq(r, p) * dm(r, r) 
		endif
	enddo
enddo


! �������������� ��������
do p = 1, nbasis
	do q = 1, nbasis
		if(p.ne.q) then
			dmprime(p, q) = (0., 0.)

			! ������������
			do r = 1, nbasis
				dmprime(p, q) = dmprime(p, q) - iunit * ( Vres_sum(p, r) * dm(r, q) -  dm(p, r) * Vres_sum(r, q) )
			enddo

			! �������������� ����
			dmprime(p, q) = dmprime(p, q) - (1./2.) * (alambda_tot(p) + alambda_tot(q)) * dm(p, q) 
		
		endif
	enddo
enddo

RETURN
!==================================================================================================================
END SUBROUTINE EqSystem_free

SUBROUTINE EqSystem_foil(t, dm, dmprime) 
!============================================================================================
! ������� ��� �������� � �������������� �������� ������
!============================================================================================
USE Global
real, intent(in) :: t
complex, dimension(nbasis, nbasis), intent(in) :: dm
complex, dimension(nbasis, nbasis), intent(out) :: dmprime

integer :: p, q, r, n


! ������� ��������������
VRes_sum = (0., 0.)

! ������� ����-������
do p = 1, nbasis
	do q = 1, nbasis
		Vres_sum(p, q) = Vres_sum(p, q) + W_ls(p, q) * cdexp(iunit * omega(p, q) * t)
	enddo
enddo


! �������� ���������
alambda_ion_heavy = 0.0
alambda_ion_elec = 0.0 
do p = 1, nbasis
	alambda_ion_heavy(p) = cs_ion_heavy_f(p) * Vion * dens_i_f * gamma
	alambda_ion_elec(p)  = cs_ion_elec_f(p)  * Vion * dens_e_f * gamma
enddo


alambda_pq = 0.0
do p = 1, nbasis
	do q = 1, nbasis
		alambda_pq(p,q) = alambda_decay(p,q) + cs_exc_heavy_f(p, q) * Vion * dens_i_f * gamma &
											 + cs_exc_elec_f(p, q)  * Vion * dens_e_f * gamma
	enddo
enddo

alambda_tot = 0.0 
do p = 1, nbasis
		alambda_tot(p) = alambda_ion_heavy(p) + alambda_ion_elec(p)	+ alambda_auto(p)
		do q = 1, nbasis
			alambda_tot(p) = alambda_tot(p) + alambda_pq(p, q)
		enddo
enddo


! ������������ ��������
do p = 1, nbasis
	dmprime(p, p) = (0., 0.)
	
	! ������������
	do r = 1, nbasis
		dmprime(p, p) = dmprime(p, p) + 2.0 * dimag( Vres_sum(p, r) * dm(r, p) )
	enddo
	

	! �������������� ����
	dmprime(p, p) = dmprime(p, p) - alambda_tot(p) * dm(p, p) 
	do r = 1, nbasis
		if(r.ne.p) then
			dmprime(p, p) = dmprime(p, p) + alambda_pq(r, p) * dm(r, r) 
		endif
	enddo
enddo


! �������������� ��������
do p = 1, nbasis
	do q = 1, nbasis
		if(p.ne.q) then
			dmprime(p, q) = (0., 0.)

			! ������������
			do r = 1, nbasis
				dmprime(p, q) = dmprime(p, q) - iunit * ( Vres_sum(p, r) * dm(r, q) -  dm(p, r) * Vres_sum(r, q) )
			enddo

			! �������������� ����
			dmprime(p, q) = dmprime(p, q) - (1./2.) * (alambda_tot(p) + alambda_tot(q)) * dm(p, q) 
		
		endif
	enddo
enddo

RETURN
!==================================================================================================================
END SUBROUTINE EqSystem_foil


SUBROUTINE Tensors(J, Dm, rho_00, rho_1q, rho_2q, rho_3q, rho_4q)
!================================================================================================================
! �������������� ������� ��� �������� ������� ���������	��������� � ������������ �������� J
! 
! Input:
! J - ������ 
! Dm(- J : J, - J : J ) - ������� ���������
!
! Output:
! ������� �������
! rho_00
! rho_1q( - 1 : 1 )
! rho_2q( - 2 : 2 )
! rho_3q( - 3 : 3 )
! rho_4q( - 4 : 4 )
!================================================================================================================
integer, intent(IN) :: J
complex, intent(IN), dimension( - J : J, - J : J ) :: Dm
complex, intent(OUT) :: rho_00
complex, intent(OUT), dimension( - 1 : 1) :: rho_1q
complex, intent(OUT), dimension( - 2 : 2) :: rho_2q
complex, intent(OUT), dimension( - 3 : 3), optional :: rho_3q
complex, intent(OUT), dimension( - 4 : 4), optional :: rho_4q

integer :: m1, m2, q

rho_00 = (0., 0.)
rho_1q = (0., 0.)
rho_2q = (0., 0.)

if (PRESENT(rho_3q)) then
	rho_3q = (0., 0.)
endif
if (PRESENT(rho_4q)) then
	rho_4q = (0., 0.)
endif

do m1 = - J, J
	do m2 = - J, J
		rho_00 = rho_00 + (-1.)**(J-m2) * CLEBSHG( 2*J, 2*m1, 2*J, - 2*m2, 0, 0 ) * Dm( m1, m2 )
	enddo
enddo

do q = - 1, 1
	do m1 = - J, J
		do m2 = - J, J
			rho_1q(q) = rho_1q(q) + (-1.)**(J-m2) * CLEBSHG( 2*J, 2*m1, 2*J, - 2*m2, 2, 2*q ) * Dm( m1, m2 )
		enddo
	enddo
enddo

do q = - 2, 2
	do m1 = - J, J
		do m2 = - J, J
			rho_2q(q) = rho_2q(q) + (-1.)**(J-m2) * CLEBSHG( 2*J, 2*m1, 2*J, - 2*m2, 4, 2*q ) * Dm( m1, m2 )
		enddo
	enddo
enddo

if (PRESENT(rho_3q)) then
	do q = - 3, 3
		do m1 = - J, J
			do m2 = - J, J
				rho_3q(q) = rho_3q(q) + (-1.)**(J-m2) * CLEBSHG( 2*J, 2*m1, 2*J, - 2*m2, 6, 2*q ) * Dm( m1, m2 )
			enddo
		enddo
	enddo
endif

if (PRESENT(rho_4q)) then
	do q = - 4, 4
		do m1 = - J, J
			do m2 = - J, J
				rho_4q(q) = rho_4q(q) + (-1.)**(J-m2) * CLEBSHG( 2*J, 2*m1, 2*J, - 2*m2, 8, 2*q ) * Dm( m1, m2 )
			enddo
		enddo
	enddo
endif

return
!================================================================================================================
END SUBROUTINE Tensors
