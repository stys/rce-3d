MODULE States
!==============================================================================
Implicit None

TYPE conf	  
	integer :: id
	integer, dimension(4) :: q1	 !|n1, l1, m1, mu1> 
	integer, dimension(4) :: q2	 !|n2, l2, m2, mu2>
END TYPE conf

TYPE state	 ! ���������� �������� ��������� �� ������������� �������������  
	integer :: status
	integer :: n1, n2, l1, l2
	integer :: L, S, J, Mj
	real :: Z1, Z2
	integer, dimension(100) :: conf_exp	! ������ 
	real, dimension(100) :: coeffs_exp	! ������������ ����������
END TYPE state

! ���� ���� ��������� ������������
type(conf), allocatable, dimension(:) :: conf_list
integer :: Nconf

CONTAINS 

	SUBROUTINE TableConf(nmax)
	!--------------------------------------------------------------
	! ����������� ���� ���� ��������� ������������
	!--------------------------------------------------------------
	integer, intent(in) :: nmax
	integer :: n1, n2
	integer :: l1, l2
	integer :: m1, m2
	integer :: mu1, mu2
	integer :: id

	id = 0
	do n1 = 1, nmax
	do l1 = 0, n1 - 1
	do m1 = - l1, l1
	do mu1 = -1, 1, 2
		do n2 = 1, nmax
		do l2 = 0, n2 - 1
		do m2 = - l2, l2
		do mu2 = -1, 1, 2
			  id = id + 1
		enddo
		enddo
		enddo
		enddo
	enddo
	enddo
	enddo
	enddo

	Nconf = id
	allocate( conf_list(Nconf) )

	print*, Nconf

	id = 0
	do n1 = 1, nmax
	do l1 = 0, n1 - 1
	do m1 = - l1, l1
	do mu1 = -1, 1, 2
		do n2 = 1, nmax
		do l2 = 0, n2 - 1
		do m2 = - l2, l2
		do mu2 = -1, 1, 2 
			  
			  id = id + 1
			  
			  conf_list(id)%id = id
			  
			  conf_list(id)%q1(1) = n1	
			  conf_list(id)%q1(2) = l1	
			  conf_list(id)%q1(3) = m1	
			  conf_list(id)%q1(4) = mu1	
			  conf_list(id)%q2(1) = n2	
			  conf_list(id)%q2(2) = l2	
			  conf_list(id)%q2(3) = m2	
  			  conf_list(id)%q2(4) = mu2	

		enddo
		enddo
		enddo
		enddo
	enddo
	enddo
	enddo
	enddo

	!--------------------------------------------------------------
	END SUBROUTINE TableConf

	INTEGER FUNCTION FindConf(q1, q2)
	!--------------------------------------------------------------
	! ������� ������ �� ������� ������ ���� ��������� ������������
	!--------------------------------------------------------------
	integer, dimension(4) :: q1, q2 
	integer :: j, m
	logical, dimension(4) :: e1, e2
	logical :: e

	
	do j = 1, Nconf
		
		e1 = q1.eq.conf_list(j)%q1 
		e2 = q2.eq.conf_list(j)%q2
		e = .true.
		do m = 1, 4
			e = e .and. e1(m) .and. e2(m)
		enddo
			
		if( e ) then
			FindConf = j
		endif
	enddo
	!--------------------------------------------------------------
	END FUNCTION FindConf

	SUBROUTINE StateConstructor(Z1, n1, l1, Z2, n2, l2, L, S, J, Mj, st)	
	!-------------------------------------------------------------------
	! ����������� ��������� |n1 l1, n2 l2, LSJMj> 
	!-------------------------------------------------------------------
	integer, intent(in)	:: n1, n2, l1, l2, L, S, J, Mj
	real, intent(in) :: Z1, Z2	
	type(state), intent(out) :: st
	real :: anorm
	integer :: pm

	integer :: m1, m2, mu1, mu2, ML, MS
	integer, dimension(4) :: q1, q2
	real :: c, c1, c2, c3
	integer :: position, id
	
	integer :: status, k	! 1 - ���� �� ��, 0 - ���� ������ 

	real :: ClebshG
	external ClebshG

	st%Z1 = Z1
	st%Z2 = Z2

	st%n1 = n1
	st%l1 = l1
	st%l2 = l2
	st%n2 = n2
	
	st%L = L
	st%S = S
	st%J = J
	st%Mj = Mj

	! ������������� �����������
	if( n1.eq.n2 .and. l1.eq.l2 ) then
		anorm = 1./2.
	else
		anorm = 1./dsqrt(2.)
	endif

	! ���� ��� �����
	pm = (- 1) ** (l1 + l2 - L - S)
	
	st%conf_exp = 0
	st%coeffs_exp = 0.0

	do m1 = - l1, l1
		do m2 = - l2, l2
			do mu1 = - 1, 1, 2
				do mu2 = - 1, 1, 2
					do ML = - L, L
						do MS = - S, S 
	
							c1 = ClebshG( 2*l1, 2*m1, 2*l2, 2*m2, 2*L, 2*ML )
							c2 = ClebshG( 1, mu1, 1, mu2, 2*S, 2*MS )
							c3 = ClebshG( 2*L, 2*ML, 2*S, 2*MS, 2*J, 2*MJ )
							c = anorm * c1 * c2 * c3
						
							if( c.ne.0.0 ) then
							
								q1(1) = n1
								q1(2) = l1
								q1(3) = m1
								q1(4) = mu1
								q2(1) = n2
								q2(2) = l2
								q2(3) = m2
								q2(4) = mu2
							
								id = FindConf(q1, q2)
								print*, id
								do position = 1, 100
									if( st%conf_exp(position).eq.id ) then
										st%coeffs_exp(position) = st%coeffs_exp(position) + c
										exit
									endif
									if( st%conf_exp(position).eq.0 ) then
										st%conf_exp(position) = id
										st%coeffs_exp(position) = c
										exit
									endif
								enddo  

								q1(1) = n2			  ! ��. �.�. ���������, 
								q1(2) = l2			  ! "�������� � ������ ������� ��������", 
								q1(3) = m2			  ! ���.123, ( 15.9 - 15.12 )
								q1(4) = mu1
								q2(1) = n1
								q2(2) = l1
								q2(3) = m1
								q2(4) = mu2
							
								id = FindConf(q1, q2)
								print*, id
								do position = 1, 100
									if( st%conf_exp(position).eq.id ) then
										st%coeffs_exp(position) = st%coeffs_exp(position) + pm * c
										exit
									endif
									if( st%conf_exp(position).eq.0 ) then
										st%conf_exp(position) = id
										st%coeffs_exp(position) = pm * c
										exit
									endif
								enddo  
													
							endif	

						enddo
					enddo
				enddo
			enddo
		enddo
	enddo

	status = 0
	do k = 1, 100
		if(st%conf_exp(k).ne.0) then
			status = 1
			exit
		endif
	enddo
	
	if(l1.gt.n1-1) status = 0
	if(l2.gt.n2-1) status = 0

	if(status.ne.0) then
		st%status = status
		st%n1 = n1
		st%n2 = n2
		st%l1 = l1
		st%l2 = l2
		st%L = L
		st%S = S
		st%J = J
		st%Mj = Mj
	else
		st%status = status
	endif	
	!---------------------------------------------------------------------------
	END SUBROUTINE StateConstructor

	REAL FUNCTION StateScalProd(st1, st2)
	!---------------------------------------------------------------------------
	! ��������� ������������ ���������
	!---------------------------------------------------------------------------
	type(state), intent(in) :: st1, st2
	integer :: j1, j2
	real :: res

	res = 0.0
	do j1 = 1, 100
		do j2 = 1, 100
			if( st1%conf_exp(j1) .eq. st2%conf_exp(j2) ) then
				res = res + st1%coeffs_exp(j1) * st2%coeffs_exp(j2)
			endif
		enddo
	enddo

	StateScalProd = res
	!---------------------------------------------------------------------------
	END FUNCTION StateScalProd

	SUBROUTINE WriteOutState(file, st)
	!-----------------------------------------------------------------------------------------------
	! ������ ���������� � ��������� � ����
	!-----------------------------------------------------------------------------------------------
	integer, intent(in) :: file
	type(state) :: st
	character*1, dimension(0:2) :: bigLname, smallLname 
	integer :: pos

	100 format ( 1X, I1, A1, 1X, I1, A1, 1X, A1, 1X, I1, A1, I1, A1, 1X, I2 ) 
	101 format ( 1X, F10.5, 1X, A1, 4(I2, 1X), A1, 1X, A1, 4(I2, 1X), A1  )

	biglname(0) = 'S'
	biglname(1) = 'P'
	biglname(2) = 'D'
	smallLname(0) = 's'
	smallLname(1) = 'p'
	smallLname(2) = 'd'

	if( st%status.ne.0 ) then
		Write(file, *)
		Write(file, *) 'Term:'	
		Write(file, 100) st%n1, smallLname(st%l1), st%n2, smallLname(st%l2), ':', &
						 2*st%S + 1, bigLname(st%L), st%J, ',', st%Mj
	
		Write(file, *) 'Expansion:'
		pos = 1
		do while( st%conf_exp(pos) .gt. 0 )
			Write(file, 101) st%coeffs_exp(pos), '|', conf_list(st%conf_exp(pos))%q1, '>', & 
												 '|', conf_list(st%conf_exp(pos))%q2, '>'
			pos = pos + 1
		enddo  
		
		Write(file, *)
		Write(file, *) 'Effective charges:'
		Write(file, *) 'Z1', st%Z1
		Write(file, *) 'Z2', st%Z2

	else
		Write(file, *)
		Write(file, *) 'Error'
	endif
	!-----------------------------------------------------------------------------------------------
	END SUBROUTINE WriteOutState

!===================================================================================================
END MODULE States

