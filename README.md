# Resonant coherent excitation in 3d

Fortran 90 codes for numerical simulation of resonant coherent excitation of relativistic hydrogenlike and heliumlike ions in crystals out of channeling conditions (3d).

### Theory
Theory behind the code was developed by Vsevolod Balashov and co-workers at Skobeltsyn Institute of Nuclear Physics from 1991 to 2011. 
 
[Final report on simulations of double RCE in 3d (eng)](http://link.springer.com/article/10.1134%2FS1063776109060120)

[Full text of PhD thesis (rus)](https://bitbucket.org/stys/rce-3d/downloads/3d-rce-thesis-ru.pdf)

### Experiments 
Three-dimensional resonant coherent excitation was discovered by experimental group from Tokyo Metropolitan University and RIKEN.   

[Discovery of 3d RCE](http://prl.aps.org/abstract/PRL/v97/i13/e135503)

[First observation of double RCE in 3d](http://prl.aps.org/abstract/PRL/v101/i11/e113201)
